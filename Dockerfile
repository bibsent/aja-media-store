# Work in progress
# https://pythonspeed.com/articles/docker-cache-insecure-images/
#
# Based on
# https://github.com/tiangolo/uvicorn-gunicorn-docker
# https://github.com/tiangolo/uvicorn-gunicorn-fastapi-docker
FROM tiangolo/uvicorn-gunicorn-fastapi:python3.8

SHELL ["/bin/bash", "-o", "pipefail", "-c"]

# Create non-privileged user
RUN useradd --system --uid 999 --user-group app \
    && mkdir -p /app-data && chown -R app:app /app-data

VOLUME ["/app-data"]

# Add CA certificate
# hadolint ignore=DL3008
RUN apt-get update && apt-get install -y --no-install-recommends \
    curl \
    netcat \
    && rm -rf /var/lib/apt/lists/* \
    && curl -sSL --fail https://static.aja.bs.no/aja-ca.crt > /usr/local/share/ca-certificates/aja-ca.crt \
    && openssl x509 -in /usr/local/share/ca-certificates/aja-ca.crt -text -noout \
    && update-ca-certificates

# Poetry
RUN curl -sSL https://raw.githubusercontent.com/python-poetry/poetry/master/install-poetry.py | POETRY_HOME=/opt/poetry python \
    && ln -s /opt/poetry/bin/poetry /usr/local/bin/poetry \
    && poetry config virtualenvs.create false

# Install Python dependencies
COPY ./pyproject.toml ./poetry.lock* /app/
RUN poetry install --no-root --no-dev --no-interaction

COPY . /app/
# Do a second poetry install pass to install the app,
# this will be much faster.
RUN poetry install --no-interaction && \
    poetry cache clear -n --all pypi

WORKDIR /app
USER app

# ENTRYPOINT ["/app/docker/entrypoint.sh"]
