"""
Pydantic schemas
"""
from __future__ import annotations
import re
from datetime import datetime
from typing import List, Optional, Any, Dict
from uuid import UUID
import structlog
from pydantic import BaseModel, Field, validator, conint, AnyUrl, root_validator

from app.settings import settings
from app.tasks import registry

log = structlog.get_logger()


class AccessTokenData(BaseModel):
    """OIDC access token claims"""
    subject: str = None  # Identifier for the user or application whose identity has been verified
    issuer: str = None  # The security token service (STS) that constructed the token, includes the tenant ID
    scopes: List[str] = Field(default_factory=list)  # The set of permissions that the requesting application or user has been given permission to
    email: Optional[str] = None  # Preferred username if the principal is a user, blank if it's an application


class User(AccessTokenData):
    pass


class OrmBase(BaseModel):
    class Config:
        orm_mode = True
        allow_population_by_field_name = True


class Asset(OrmBase):
    """Asset output schema"""
    asset_type: str = Field(example='cover')
    created: datetime
    version: int
    source: Optional[str]
    width: int = Field(..., example=800)
    height: int = Field(..., example=1200)
    size: int = Field(..., example=123923)
    format: str = Field(..., example='JPEG')
    s3_version: str = Field(...)
    md5: str = Field(...)

    url: AnyUrl


class Product(OrmBase):
    uuid: UUID
    ean: Optional[str] = Field(example='9788272016844')

    @validator('ean')
    def ean_should_be_valid(cls, v):
        if v is None:
            return None
        if len(v) != 13:
            raise ValueError('EAN must be 13 characters long')
        return v


class Tag(OrmBase):
    key: str
    value: str


class AssetIn(BaseModel):
    """Asset input schema"""
    ean: str
    asset_type: str
    source: str
    file: Any
    product_uuid: Optional[UUID] = None
    uploader: Optional[User] = None

    @validator('file')
    def valid_file(cls, v):
        if not hasattr(v, 'read'):
            raise ValueError('must be file-like')
        return v

    @validator('ean')
    def valid_ean(cls, v):
        # Note: We don't make an effort of validating the checksum here, since publishers sometimes
        # use invalid ISBNs and we don't validate them anywhere else at the moment.
        v = v.replace('-', '')
        if not re.match(r'^[0-9]{13}$', v):
            raise ValueError('must be 13 digits long')
        return v

    @validator('asset_type')
    def valid_asset_type(cls, v):
        valid_types = {'cover'}  # at the moment, but might add more in the future
        if v not in valid_types:
            raise ValueError('must be one of: ("%s")' % '", "'.join(valid_types))
        return v

    @validator('source')
    def valid_source(cls, v):
        if not v.isascii() or not v.isprintable():
            # Reason for this is that we get `botocore.exceptions.ParamValidationError`
            # for non-ascii characters: S3 metadata can only contain ascii.
            raise ValueError('can only contain printable ascii characters ')
        return v


class AssetVariant(OrmBase):
    created: datetime
    name: str = Field(..., example='thumbnail.jpg')
    width: int = Field(..., example=800)
    height: int = Field(..., example=1200)
    size: int = Field(..., example=123923)
    format: str = Field(..., example='JPEG')


class SimpleAssetResponse(Asset):
    product: Product


class ExportStatusResponse(OrmBase):
    target: str = Field(...)
    date: datetime = Field(..., example='2021-09-12T16:06:00')
    extra: Any = Field(...)


class AssetResponse(SimpleAssetResponse):
    variants: List[AssetVariant]
    export_status: List[ExportStatusResponse]

    # uri: Optional[str]

    # ... 'pimcore_api'?
    # kanskje dominant color fra https://github.com/fengsp/color-thief-py
    #
    # @validator("metadata", pre=True)
    # def evaluate_metadata(cls, value):
    #     print("EVAL", value)
    #     return value
    #     return {item.key: item.value for item in value}
    #     #if isinstance(value, Query):
    #     #    return [tag.value for tag in value.all()]
    #     #return value


class HealthResponse(BaseModel):
    status: str = 'pass'


class StatsData(BaseModel):
    total: int = Field(..., example=1203178)
    by_isbn: Dict[str, int] = Field(..., example={"978": 801992, "97882": 26781})
    by_source: Dict[str, int] = Field(..., example={"aja_sftp": 2986, "gardners": 670183})


class StatsResponse(BaseModel):
    date: datetime = Field(..., example='2021-09-12T16:06:00')
    data: StatsData


class JobStatus(BaseModel):
    job_id: str
    enqueued_at: datetime
    started_at: Optional[datetime]
    ended_at: Optional[datetime]
    state: str
    retries: int
    result: dict


class JobsResponse(BaseModel):
    jobs: Dict[str, List[JobStatus]]


class HealthResponseFail(BaseModel):
    status: str = 'fail'


class ProductResponse(OrmBase):
    uuid: str
    ean: str
    url: AnyUrl
    assets: Dict[str, Asset]


class Message(BaseModel):
    message: str


class VariantConfig(BaseModel):
    width: conint(gt=0, le=settings.max_thumbnail_dim) = settings.max_thumbnail_dim
    height: conint(gt=0, le=settings.max_thumbnail_dim) = settings.max_thumbnail_dim
    format: str = 'JPEG'
    mime_type: str = 'image/jpeg'


class JobDefinition(BaseModel):
    job: str = Field(..., example="promus_export")
    arguments: Dict[str, Any] = Field(..., example='{"ean": "9788299990356"}')

    @validator('job')
    def valid_job_name(cls, v):
        valid_names = set(registry.all().keys())
        if v not in valid_names:
            raise ValueError("invalid job name")
        return v

    @root_validator(pre=False, skip_on_failure=True)
    def valid_job_arguments(cls, values):
        job = registry.get(values["job"])
        job.input_model(**values["arguments"])
        return values
