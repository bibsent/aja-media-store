import functools
from typing import Iterator

import boto3
import requests
from mypy_boto3_s3 import S3ServiceResource, S3Client
from mypy_boto3_s3.service_resource import Bucket
from sqlalchemy.orm import Session

from app.database import db_session
from app.settings import settings


def get_db() -> Iterator[Session]:
    session: Session = db_session()
    try:
        yield session
    finally:
        session.close()


def get_s3_client() -> S3Client:
    return boto3.client(
        "s3",
        endpoint_url=settings.s3_endpoint_url,
        region_name=settings.s3_region,
        aws_access_key_id=settings.s3_access_key,
        aws_secret_access_key=settings.s3_access_secret
    )


def get_s3() -> S3ServiceResource:
    return boto3.resource(
        "s3",
        endpoint_url=settings.s3_endpoint_url,
        region_name=settings.s3_region,
        aws_access_key_id=settings.s3_access_key,
        aws_secret_access_key=settings.s3_access_secret
    )


def get_s3_bucket() -> Bucket:
    return boto3.resource(
        "s3",
        endpoint_url=settings.s3_endpoint_url,
        region_name=settings.s3_region,
        aws_access_key_id=settings.s3_access_key,
        aws_secret_access_key=settings.s3_access_secret
    ).Bucket(settings.s3_bucket)


def get_http_session():
    """
    Get a session with a default timeout, to avoid blocking indefinitely.
    Until https://github.com/psf/requests/pull/5230 or something similar is merged, the easiest
    approach is to shim the request method. See also https://github.com/psf/requests/issues/3070
    """
    session = requests.Session()
    connect_timeout, read_timeout = 10.0, 60.0
    session.request = functools.partial(session.request, timeout=(connect_timeout, read_timeout))
    return session
