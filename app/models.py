"""
SQL Alchemy models
"""
from __future__ import annotations

import os.path
from dataclasses import field, dataclass
from time import time

import structlog
from sqlalchemy import Column, Unicode, DATETIME, ForeignKey, Integer, JSON
from sqlalchemy.dialects import mysql
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.orm import relationship
from sqlalchemy.orm.collections import attribute_mapped_collection

from app.settings import settings

Base = declarative_base()

log = structlog.get_logger(__name__)


class Product(Base):
    __tablename__ = "product"
    # Why not UUID for primary key?
    # https://www.percona.com/blog/2019/11/22/uuids-are-popular-but-bad-for-performance-lets-discuss/
    id = Column(Integer, primary_key=True)
    uuid = Column(Unicode(36), index=True)
    ean = Column(Unicode(13), nullable=True, index=True)
    created = Column(DATETIME, index=True)

    assets = relationship("Asset",
                          back_populates="product",
                          collection_class=attribute_mapped_collection('asset_type'),  # Only return the latest version of each type
                          order_by="Asset.version"
                          )  # lazy='dynamic'?
    events = relationship("Event", back_populates="product")  # lazy='dynamic'?
    export_status = relationship("ExportStatus", back_populates="product")  # lazy='dynamic'?

    @property
    def url(self):
        return os.path.join(settings.asset_base_url, self.uuid)


class User(Base):
    __tablename__ = "user"
    id = Column(Integer, primary_key=True)
    external_id = Column(Unicode(100), nullable=True)
    issuer = Column(Unicode(255), nullable=True)
    username = Column(Unicode(100), nullable=True)

    uploads = relationship("Asset", back_populates="uploader")  # lazy='dynamic'?


class Asset(Base):
    __tablename__ = "asset"
    id = Column(Integer, primary_key=True)
    product_id = Column(Integer, ForeignKey("product.id", ondelete="CASCADE"), nullable=False, index=True)
    asset_type = Column(Unicode(200))  # 'front_cover'
    created = Column(DATETIME, index=True)
    version = Column(Integer)
    s3_version = Column(Unicode(50))
    md5 = Column(Unicode(50))
    source = Column(Unicode(100))
    format = Column(Unicode(5))
    width = Column(mysql.INTEGER(6))
    height = Column(mysql.INTEGER(6))
    size = Column(mysql.INTEGER(10))
    uploader_id = Column(Integer, ForeignKey("user.id", ondelete="SET NULL"), nullable=True, index=True)

    product = relationship("Product", back_populates="assets")  # lazy='dynamic'?
    variants = relationship("AssetVariant", back_populates="asset")  # lazy='dynamic'?
    export_status = relationship("ExportStatus", back_populates="asset")  # lazy='dynamic'?
    uploader = relationship("User", back_populates="uploads")  # lazy='dynamic'?

    @property
    def url(self):
        return os.path.join(settings.asset_base_url, self.product.uuid, self.asset_type, "original.jpg")

    # def as_dict(self):
    #     return {c.key: getattr(self, c.key)
    #             for c in inspect(self).mapper.iterate_properties}

    # def __repr__(self):
    #    return "<Asset(uuid='%s', asset_type='%s', tags=%s)>" % (self.uuid, self.asset_type, ','.join(self.tags))


class AssetVariant(Base):
    __tablename__ = "variant"
    id = Column(Integer, primary_key=True)
    asset_id = Column(Integer, ForeignKey("asset.id", ondelete="CASCADE"), nullable=False, index=True)
    name = Column(Unicode(100))
    created = Column(DATETIME)
    format = Column(Unicode(5))
    width = Column(mysql.INTEGER(6))
    height = Column(mysql.INTEGER(6))
    size = Column(mysql.INTEGER(10))
    asset = relationship("Asset", back_populates="variants")  # lazy='dynamic'?


class Event(Base):
    __tablename__ = "event"
    id = Column(Integer, primary_key=True)
    product_id = Column(Integer, ForeignKey("product.id", ondelete="CASCADE"), nullable=False, index=True)
    event_type = Column(Unicode(50), nullable=False)
    details = Column(Unicode(255), nullable=True)  # details?

    product = relationship("Product", back_populates="events")  # lazy='dynamic'?


@dataclass
class ImportStatus:
    stats: dict = field(default_factory=lambda: {"created": 0, "updated": 0, "unchanged": 0})
    timing: dict = field(default_factory=dict)
    metadata: dict = field(default_factory=dict)

    def set_metadata(self, key, value):
        self.metadata[key] = value

    def start(self):
        self._start_time = time()
        self._task_time = time()
        return self

    def time(self, task):
        t1 = time()
        self.timing[task] = t1 - self._task_time
        self._task_time = t1

    def complete(self):
        self.timing['total'] = time() - self._start_time

    def incr(self, series):
        self.stats[str(series)] += 1


class ExportStatus(Base):
    __tablename__ = "export_status"
    id = Column(Integer, primary_key=True)
    target = Column(Unicode(50), nullable=False)
    date = Column(DATETIME, nullable=False)
    extra = Column(JSON, default={}, nullable=False)
    product_id = Column(Integer, ForeignKey("product.id", ondelete="CASCADE"), nullable=True, index=True)
    asset_id = Column(Integer, ForeignKey("asset.id", ondelete="CASCADE"), nullable=True, index=True)
    product = relationship("Product", back_populates="export_status")  # lazy='dynamic'?
    asset = relationship("Asset", back_populates="export_status")  # lazy='dynamic'?


class Stats(Base):
    __tablename__ = "stats"
    id = Column(Integer, primary_key=True)
    date = Column(DATETIME, nullable=False)
    data = Column(JSON, default={}, nullable=False)


class JobStatus(Base):
    __tablename__ = "job_status"
    job_id = Column(Unicode(40), primary_key=True)
    job_name = Column(Unicode(50), nullable=False)
    enqueued_at = Column(DATETIME, nullable=True)
    started_at = Column(DATETIME, nullable=True)
    ended_at = Column(DATETIME, nullable=True)
    result = Column(JSON, default={}, nullable=False)
    retries = Column(Integer, default=0, nullable=False)
    total_processed = Column(Integer, default=0, nullable=False)
    state = Column(Unicode(20), nullable=True)


class JobState(Base):
    __tablename__ = "job_state"
    job_name = Column(Unicode(50), primary_key=True)
    state = Column(JSON, default={}, nullable=False)
