"""
Way too simple event system.
"""
from typing import Callable

import structlog
from pydantic import BaseModel

log = structlog.get_logger(__name__)

_listeners = []

ASSET_CREATED = 'asset_created'


class Listener(BaseModel):
    event_name: str
    callback_fn: Callable


def listen(event_name, callback_fn):
    _listeners.append(Listener(event_name=event_name, callback_fn=callback_fn))


def emit(event_name, *args, **kwargs):
    log.debug('Emit event: %s' % event_name)
    for listener in _listeners:
        if listener.event_name == event_name:
            listener.callback_fn(*args, **kwargs)
