import json
import os
import re
from datetime import datetime, timezone
from io import BytesIO, RawIOBase
from time import time
from typing import Any, cast, Optional
from io import BufferedReader

import botocore.exceptions
import pydantic
from fastapi import Response, Security
from fastapi.encoders import jsonable_encoder
from fastapi_pagination import LimitOffsetPage, add_pagination
from fastapi_pagination.ext.sqlalchemy import paginate
from mypy_boto3_s3.service_resource import Bucket
from fastapi import FastAPI, UploadFile, File, Form, Depends, Path, HTTPException, Query
from fastapi.openapi.utils import get_openapi
from fastapi.staticfiles import StaticFiles
from PIL import Image
from sentry_sdk.integrations.asgi import SentryAsgiMiddleware
from sqlalchemy import text
from sqlalchemy.exc import OperationalError

from sqlalchemy.orm import Session, contains_eager, selectinload
from starlette.middleware.cors import CORSMiddleware
from starlette.responses import StreamingResponse, JSONResponse, RedirectResponse
from starlette_context.plugins import RequestIdPlugin
from starlette_context.middleware import RawContextMiddleware
import structlog

from app import schemas, models, dependencies
from app.security import get_current_user
from app.services import asset
from app import tasks

from app.logging import configure_logging
from app.settings import settings
from app.sentry import configure_sentry
from app.tasks import registry


app = FastAPI()
app.mount("/static", StaticFiles(directory="static"), name="static")

# Add X-Request-Id header value to log entries
app.add_middleware(RawContextMiddleware, plugins=(
    RequestIdPlugin(force_new_uuid=False, validate=False),
))

app.add_middleware(
    CORSMiddleware,
    allow_origins=['*'],
    allow_credentials=True,
    allow_methods=["*"],
    allow_headers=["*"],
)

app.add_middleware(
    SentryAsgiMiddleware,
)

log = structlog.get_logger(__name__)


class NonCloseableBufferedReader(BufferedReader):
    def __init__(self, raw: Any, *args: Any, **kwargs: Any):
        super().__init__(cast(RawIOBase, raw), *args, **kwargs)

    def close(self):
        self.flush()


@app.on_event("startup")
def startup():
    configure_logging()
    log.info('Web API starting')
    Image.MAX_IMAGE_PIXELS = settings.MAX_IMAGE_PIXELS
    configure_sentry()
    tasks.configure_task_broker()


@app.on_event("shutdown")
async def shutdown():
    log.info('App shutdown')
    # await sessionmaker.shutdown?
    #   database.disconnect()


@app.get("/health", response_model=schemas.HealthResponse, responses={503: {"model": schemas.HealthResponseFail}},
         tags=["service"])
async def get_health(db: Session = Depends(dependencies.get_db)):
    """Check if the API is healthy."""
    try:
        with db.connection():
            pass
    except OperationalError as exc:
        log.error("Health check failed", error=str(exc))
        return JSONResponse(status_code=503, content={"status": "fail", "output": str(exc)})

    return {'status': 'pass'}


@app.get(
    "/jobs",
    tags=["jobs"],
    response_model=schemas.JobsResponse,
    dependencies=[Security(get_current_user)]
)
def get_jobs_status(
    db: Session = Depends(dependencies.get_db)
):
    """Get status information about the last 10 job executions of each job type"""
    out = {"jobs": {}}

    res = db.execute(text("""
        SELECT job_name, job_id, enqueued_at, started_at, ended_at, state, result, retries FROM
        (
            SELECT
              *,
              row_number() over (partition by job_name
                                 order by enqueued_at desc) as my_rank
            FROM job_status
            WHERE state <> 'COMPLETED' OR total_processed > 0
        ) t
        WHERE my_rank <= 10
    """)).fetchall()
    for row in res:
        if row[0] not in out["jobs"]:
            out["jobs"][row["job_name"]] = []
        out["jobs"][row["job_name"]].append({
            "job_id": row["job_id"],
            "enqueued_at": row["enqueued_at"],
            "started_at": row["started_at"],
            "ended_at": row["ended_at"],
            "state": row["state"],
            "retries": row["retries"],
            "result": json.loads(row["result"]),
        })

    return schemas.JobsResponse(**out)


@app.get(
    "/stats",
    tags=["service"],
    response_model=schemas.StatsResponse,
    dependencies=[Security(get_current_user)]
)
def get_stats(
    db: Session = Depends(dependencies.get_db)
):
    db_stats = db.query(models.Stats).order_by(models.Stats.date.desc()).first()
    return schemas.StatsResponse(**db_stats.__dict__)


@app.get("/", include_in_schema=False)
def get_index():
    return RedirectResponse(url='/docs')


@app.get("/jobs/job-types",
         tags=["jobs"]
         )
def list_jobtypes():
    return {
        'job-types': list(registry.all().keys()),
    }


@app.post("/jobs/", tags=["jobs"])
def execute_job(
    definition: schemas.JobDefinition,
    current_user: schemas.User = Security(get_current_user, scopes=["execute:jobs"]),
):
    job = registry.get(definition.job)
    message = job.send(**definition.arguments)
    log.info(f"Job '{definition.job}' started by user '{current_user.email}', job id: {message.message_id}")
    return {"job_id": message.message_id}


@app.get("/assets/",
         response_model=LimitOffsetPage[schemas.SimpleAssetResponse],
         responses={303: {"description": "Redirect to the asset, if variant is specified"}},
         tags=["assets"],
         dependencies=[Security(get_current_user, scopes=['read:metadata'])]
         )
def find_assets(
    response: Response,
    db: Session = Depends(dependencies.get_db),
    product_uuid: Optional[str] = Query(None, description="Product UUID", example="f1e96b8b-9c57-4a76-a7d3-1267e0f57ee1"),
    ean: Optional[str] = Query(None, description="Product ISBN or other EAN", example="9788248928393"),
    asset_type: Optional[str] = Query(None, description="Asset type", example='cover'),
    variant: Optional[str] = Query(None, description="Variant: Request a specific variant (size, format) of the first located asset.", example='thumbnail.jpg'),
):
    """
    Find assets by some criteria (UUID or EAN). If asset_type and variant is specified, the API returns
    a redirect to that specific asset. Otherwise, it returns a paginated list, sorted by descending creation date.
    """
    query = asset.find(db, asset_type=asset_type, product_uuid=product_uuid, ean=ean)

    if variant is not None:
        db_asset = query.first()
        if db_asset is None:
            return RedirectResponse(os.path.join(settings.asset_base_url, 'static/asset_not_found.png'), status_code=303)

        return RedirectResponse(db_asset.url, status_code=303)

    t0 = time()
    page = paginate(query)
    response.headers["Server-Timing"] = "db;dur=%.1f" % ((time() - t0) * 1000)
    return page


@app.get("/products/", response_model=LimitOffsetPage[schemas.ProductResponse],
         tags=["assets"], deprecated=True, dependencies=[Security(get_current_user, scopes=['read:metadata'])])
def find_products(
    response: Response,
    db: Session = Depends(dependencies.get_db),
    uuid: Optional[str] = None,
    ean: Optional[str] = None,
):
    """Find products by some criteria."""

    query = db.query(models.Product) \
        .options(selectinload(models.Product.assets)) \
        .order_by(models.Product.created.desc())
    if uuid is not None:
        query = query.filter(models.Product.uuid == uuid)
    if ean is not None:
        query = query.filter(models.Product.ean == ean.replace('-', ''))

    t0 = time()
    page = paginate(query)
    response.headers["Server-Timing"] = "db;dur=%.1f" % ((time() - t0) * 1000)
    return page


def log_and_fail(msg, **kwargs):
    log.error(msg, **kwargs)
    return JSONResponse({"message": msg}, status_code=500)


@app.get("/user", response_model=schemas.User,
         tags=["users"])
def get_user(
    current_user: schemas.User = Security(get_current_user),
):
    """Return the authenticated user, useful to check if you are authenticated and which scopes you have."""
    return current_user


@app.post("/assets/", response_model=schemas.AssetResponse,
          responses={400: {'model': schemas.Message}},
          tags=["assets"])
def store_asset(
    uploader: schemas.User = Security(get_current_user, scopes=['upload:files']),
    db: Session = Depends(dependencies.get_db),
    bucket: Bucket = Depends(dependencies.get_s3_bucket),
    file: UploadFile = File(...),
    asset_type: str = Form(...),
    ean: str = Form(...),
    source: str = Form(...),
    product_uuid: Optional[str] = Form(None),
):
    """Store an asset and associate it with a product, specified by either `isbn` or `product_uuid`.
    If an asset of the same `asset_type` already exists on the product, the new asset will be stored as a new version.

    Requires an access token with the 'upload:files' scope."""
    try:
        return asset.store(
            db,
            bucket,
            schemas.AssetIn(
                file=file.file,
                asset_type=asset_type,
                ean=ean,
                source=source,
                product_uuid=product_uuid,
                uploader=uploader
            )
        )['asset']
    except pydantic.ValidationError as err:
        raise HTTPException(status_code=422, detail=jsonable_encoder(err.errors()))
    except asset.AssetStoreException as exc:
        return log_and_fail(str(exc), ean=ean, product_uuid=product_uuid, source=source, asset_type=asset_type)


@app.get("/{product_uuid}/{asset_type}", response_model=schemas.AssetResponse,
         tags=["assets"], dependencies=[Security(get_current_user, scopes=['read:metadata'])])
def get_product_asset(
    db: Session = Depends(dependencies.get_db),
    product_uuid: str = Path(...),
    asset_type: str = Path(...)
):
    db_asset = db.query(models.Asset)\
        .join(models.Asset.product)\
        .join(models.Asset.export_status, isouter=True)\
        .filter(models.Product.uuid == product_uuid)\
        .filter(models.Asset.asset_type == asset_type)\
        .order_by(models.Asset.version.desc())\
        .options(contains_eager(models.Asset.product))\
        .options(contains_eager(models.Asset.export_status))\
        .first()

    if db_asset is None:
        return JSONResponse({"error": "No such asset"}, status_code=404)

    return db_asset


@app.get("/{product_uuid}/{asset_type}/{variant}",
         tags=["assets"])  # , response_model=schemas.Asset)
def get_product_asset_variant(
    bucket: Bucket = Depends(dependencies.get_s3_bucket),
    product_uuid: str = Path(...),
    asset_type: str = Path(...),
    variant: str = Path(...),
    db: Session = Depends(dependencies.get_db),
):
    """Get a variant of an asset associated with a product."""
    try:
        variant_configurations = {
            'original.jpg': schemas.VariantConfig(),
            'thumbnail.jpg': schemas.VariantConfig(width=450, height=450),
        }
        # Validate variant
        if variant in variant_configurations:
            variant_config = variant_configurations[variant]
        elif match := re.match(r'h([1-9][0-9]+)\.jpg', variant):
            # JPEG version of the image with a custom thumbnail size
            variant_config = schemas.VariantConfig(width=int(match.group(1)), height=int(match.group(1)))
        else:
            return JSONResponse({"error": "Unknown variant"}, status_code=400)

        # Check if variant already exists
        variant_obj = bucket.Object('variants/%s/%s/%s' % (product_uuid, asset_type, variant))
        try:
            variant_resp = variant_obj.get()
            if variant_resp['ContentLength'] > 0 and 'ETag' in variant_resp:
                # ContentLength can be zero if the last thumbnail creation failed
                return StreamingResponse(variant_resp['Body'], media_type=variant_config.mime_type, headers={
                    'Content-Length': str(variant_resp['ContentLength']),
                    'Last-Modified': variant_resp['LastModified'].strftime('%a, %d %b %Y %I:%M:%S %Z'),
                    'ETag': variant_resp['ETag'],
                    'Cache-Control': 'public, max-age=14400',
                })
        except botocore.exceptions.ClientError as exception:
            if exception.response['Error']['Code'] != 'NoSuchKey':
                return log_and_fail('Failed to get asset variant from storage: ' + str(exception))

        obj = bucket.Object('originals/%s/%s' % (product_uuid, asset_type))
        try:
            obj_response = obj.get()
        except botocore.exceptions.ClientError as exception:
            if exception.response['Error']['Code'] != 'NoSuchKey':
                return JSONResponse({"error": "No such asset"}, status_code=404)
            return log_and_fail('Failed to get asset original from storage: ' + str(exception))

        out = BytesIO()

        with Image.open(BytesIO(obj_response['Body'].read())) as im:
            im.thumbnail((variant_config.width, variant_config.height))
            if im.mode == "RGBA":
                # Convert RGBA to RGB, so it can be saved as JPEG
                background = Image.new('RGB', im.size, (255, 255, 255))
                background.paste(im, mask=im.getchannel("A"))
                im = background
            elif im.mode == "P":
                # Convert from palette mode to RGB, so it can be saved as JPEG
                im = im.convert("RGB")
            im.save(out, variant_config.format, quality=85)
            thumbnail_size = im.size

        db_asset = db.query(models.Asset)\
            .join(models.Asset.product)\
            .filter(models.Product.uuid == product_uuid)\
            .filter(models.Asset.asset_type == asset_type)\
            .order_by(models.Asset.version.desc()) \
            .options(contains_eager(models.Asset.product)) \
            .first()

        if db_asset is None:
            return JSONResponse({"error": "No such asset"}, status_code=404)

        db_variant = models.AssetVariant(
            asset=db_asset,
            name=variant,
            format=variant_config.format,
            created=datetime.now(),
            width=thumbnail_size[0],
            height=thumbnail_size[1],
            size=out.getbuffer().nbytes,
        )
        db.commit()
        db.refresh(db_variant)
        log.info("Stored asset variant", product_uuid=product_uuid, asset_id=db_variant.asset_id,
                 variant_id=db_variant.id)

        out.seek(0)

        now = datetime.now(timezone.utc).strftime('%a, %d %b %Y %I:%M:%S %Z')

        # Workaround: https://github.com/boto/s3transfer/issues/80#issuecomment-482534256
        buffer = NonCloseableBufferedReader(out)
        variant_obj.upload_fileobj(buffer)
        buffer.detach()
        out.seek(0)

        # We could get the etag, but requires another request
        # variant_resp = variant_obj.get()
        return StreamingResponse(out, media_type=variant_config.mime_type, headers={
            'Content-Length': str(out.getbuffer().nbytes),
            'X-Media-Store-Cached': 'no',
            'Last-Modified': now,
            'Cache-Control': 'public, max-age=14400',
        })
    except pydantic.ValidationError as err:
        raise HTTPException(status_code=422, detail=jsonable_encoder(err.errors()))


def openapi_definition():
    if app.openapi_schema:
        return app.openapi_schema
    openapi_schema = get_openapi(
        title="Media store API",
        version="1.0.0",
        description="API for storing and retrieving media files associated with publications/products.",
        routes=app.routes,
    )
    openapi_schema["info"]["x-logo"] = {
        "url": "https://pim.bibsent.no/nedlastinger/logo-og-profil/Biblioteksentralen_b.jpg"
    }
    app.openapi_schema = openapi_schema
    return app.openapi_schema


app.openapi = openapi_definition
add_pagination(app)
