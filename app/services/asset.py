from datetime import datetime
import enum
from uuid import uuid4

import botocore.exceptions
import structlog
from PIL import Image
from mypy_boto3_s3.service_resource import Bucket
from sqlalchemy.orm import Session, selectinload

from app import models, event, schemas
from app.security import find_or_create_db_user
from app.settings import settings

log = structlog.get_logger(__name__)


class ImageStatus(enum.Enum):
    CREATED = enum.auto()
    UPDATED = enum.auto()
    UNCHANGED = enum.auto()

    def __str__(self):
        return self.name.lower()


class AssetStoreException(Exception):
    pass


def store(db: Session, bucket: Bucket, input: schemas.AssetIn):
    asset_version = 1
    old_db_asset = None
    old_etag = ''
    old_size = 0
    old_source = ''
    image_status = ImageStatus.CREATED

    # Determine uploader
    if input.uploader is not None:
        uploader = find_or_create_db_user(db, input.uploader)
    else:
        uploader = None

    # 1. Find or create product AND look for existing exist (two concerns, really)
    db_product = db.query(models.Product).filter_by(ean=input.ean).first()
    if db_product is None:
        if input.product_uuid:
            if db.query(models.Product).filter_by(uuid=input.product_uuid).first() is not None:
                raise AssetStoreException(
                    'Invalid request. A product with EAN %s does not exist, but a product with UUID %s exists' % (input.ean, input.product_uuid)
                )
            product_uuid = str(input.product_uuid)
        else:
            product_uuid = str(uuid4())

        db_product = models.Product(ean=input.ean, uuid=product_uuid, created=datetime.now())
        db.add(db_product)
        db.add(models.Event(product=db_product, event_type="PRODUCT_CREATE"))
    else:
        old_db_asset = db.query(models.Asset)\
            .filter_by(product=db_product, asset_type=input.asset_type)\
            .order_by(models.Asset.version.desc())\
            .first()

        if old_db_asset is not None:
            asset_version = old_db_asset.version + 1
            old_source = old_db_asset.source
            image_status = ImageStatus.UPDATED

    # 2. Check if key exists in S3 store
    original_key = 'originals/%s/%s' % (db_product.uuid, input.asset_type)
    original_obj = bucket.Object(original_key)
    try:
        # In case key already exists, get current etag to check if the contents change after upload
        original_obj.load()
    except botocore.exceptions.ClientError:
        pass
    else:
        # Incomplete transfers will not have MD5/e-tag,
        if original_obj.e_tag:
            old_etag = original_obj.e_tag.strip('"')
            old_size = original_obj.content_length

    # 3. Open image
    Image.MAX_IMAGE_PIXELS = settings.MAX_IMAGE_PIXELS

    # noinspection PyBroadException
    # Pillow doesn't have a base class for the exceptions we might encounter, such as PIL.Image.DecompressionBombError,
    # PIL.Image.UnidentifiedImageError, etc.
    try:
        im = Image.open(input.file)
    except Exception as exc:
        raise AssetStoreException('Failed to load image: ' + str(exc))

    # 4. Upload image
    input.file.seek(0)
    original_obj.upload_fileobj(
        Fileobj=input.file,
        ExtraArgs={
            "Metadata": {
                "product_uuid": db_product.uuid,
                "product_ean": db_product.ean,
                "asset_type": input.asset_type,
                "asset_source": input.source,
            }
        }
    )

    # 5. Check upload status
    try:
        original_obj.wait_until_exists(IfNoneMatch=old_etag)
    except botocore.exceptions.WaiterError as exc:
        if exc.last_response and exc.last_response['Error']['Code'] == '304':
            # Note: if exactly the same file is uploaded again
            if old_db_asset:
                if old_db_asset.source != input.source:
                    log.warning('Received unmodified copy of existing asset',
                                ean=input.ean, uuid=db_product.uuid, source=input.source, old_source=old_db_asset.source)
                    image_status = ImageStatus.UNCHANGED
                else:
                    log.warning('Ignoring unmodified copy of existing asset from same source',
                                ean=input.ean, uuid=db_product.uuid, source=input.source, old_source=old_db_asset.source)
                    return {
                        'asset': old_db_asset,
                        'status': ImageStatus.UNCHANGED,
                    }
            else:
                log.warning(
                    "Integrity problem: The exact file already exists in S3, but not in DB."
                    "Most likely the last transfer did not complete. Will add asset to DB.",
                    ean=input.ean, product_uuid=db_product.product_uuid, source=input.source
                )
        else:
            raise AssetStoreException("Failed to store asset in S3: " + str(exc))

    original_obj.reload()
    size = original_obj.content_length

    if original_obj.version_id is None:
        raise AssetStoreException('Versioning is not enabled for this bucket')

    # 6. Store new metadata
    if old_etag == '':
        log.info('Stored new asset',
                 asset_type=input.asset_type,
                 uuid=db_product.uuid, ean=db_product.ean,
                 size=size, source=input.source)
    elif image_status == ImageStatus.UNCHANGED:
        log.info('Stored new version of (unchanged) asset',
                 uuid=db_product.uuid, ean=db_product.ean,
                 old_size=old_size, size=size,
                 old_source=old_source, source=input.source)
    else:
        log.info('Stored new version of asset',
                 uuid=db_product.uuid, ean=db_product.ean,
                 old_size=old_size, size=size,
                 old_source=old_source, source=input.source)

    db_asset = models.Asset(
        product=db_product,
        asset_type=input.asset_type,
        created=datetime.now(),
        version=asset_version,
        s3_version=original_obj.version_id,
        md5=original_obj.e_tag.strip('"'),
        source=input.source,
        format=im.format,
        width=im.size[0],
        height=im.size[1],
        size=size,
        uploader=uploader
    )

    db.commit()
    db.refresh(db_asset)

    event.emit(event.ASSET_CREATED, db_asset)

    # Invalidate variants
    bucket.objects.filter(Prefix='%s/%s/variants' % (db_product.uuid, input.asset_type)).delete()

    return {
        'asset': db_asset,
        'status': image_status,
    }


def find(db, asset_id=None, asset_type=None, product_uuid=None, ean=None):
    query = db.query(models.Asset)\
        .options(selectinload(models.Asset.product))\
        .order_by(models.Asset.created.desc())

    if asset_id is not None:
        query = query.filter(models.Asset.id == asset_id)
    if asset_type is not None:
        query = query.filter(models.Asset.asset_type == asset_type)
    if product_uuid is not None:
        query = query.join(models.Asset.product).filter(models.Product.uuid == product_uuid)
    if ean is not None:
        query = query.join(models.Asset.product).filter(models.Product.ean == ean.replace('-', ''))

    return query
