import logging

import sqlalchemy
from sqlalchemy.orm import sessionmaker

from app.settings import settings


engine = sqlalchemy.create_engine(
    settings.db_url,
    pool_pre_ping=True,
    echo=True if settings.log_level <= logging.DEBUG else False
)

# Session factory
db_session = sessionmaker(bind=engine,
                          autocommit=False)
