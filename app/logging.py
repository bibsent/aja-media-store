import logging
import structlog
from starlette_context import context
from app.settings import settings
from structlog.types import EventDict

root_logger = logging.getLogger()


def add_extra_context(keys: set):
    """Extract extra key-values from the standard logger record and populate the `event_dict` with them."""
    def fn(
        logger: logging.Logger, method_name: str, event_dict: EventDict
    ) -> EventDict:
        record: logging.LogRecord = event_dict.get("_record")
        if record is not None:
            for key in keys:
                event_dict[key] = getattr(record, key)
        return event_dict
    return fn


def add_app_context(
    logger: logging.Logger, method_name: str, event_dict: EventDict,
) -> EventDict:
    """Add Starlette application context"""
    if context.exists():
        event_dict.update(context.data)
    return event_dict


def configure_logging():
    import logging.config
    pre_chain = [
        structlog.processors.TimeStamper(fmt="iso"),  # "%Y-%m-%d %H:%M:%S")
        structlog.stdlib.add_log_level,
        structlog.stdlib.add_logger_name,
        add_app_context,
        add_extra_context({'process', 'threadName'}),
        structlog.threadlocal.merge_threadlocal,  # to allow for bind_threadlocal
    ]

    # root_logger.handlers = []

    logging.config.dictConfig({
        "version": 1,
        "disable_existing_loggers": False,
        "formatters": {
            "json": {
                "()": structlog.stdlib.ProcessorFormatter,
                "processor": structlog.processors.JSONRenderer(),
                "foreign_pre_chain": pre_chain,
            },
        },
        "handlers": {
            "json": {
                "level": settings.log_level,
                "class": "logging.StreamHandler",
                "formatter": "json",
            },
        },
        "loggers": {
            "": {
                "handlers": ["json"],
                "level": settings.log_level,
            },
        },
    })

    structlog.configure(
        processors=pre_chain + [
            structlog.stdlib.ProcessorFormatter.wrap_for_formatter,
        ],
        logger_factory=structlog.stdlib.LoggerFactory(),
    )

    # renderer = structlog.processors.JSONRenderer()
    #
    # formatter = structlog.stdlib.ProcessorFormatter(
    #     processor=renderer,
    #     foreign_pre_chain=shared_processors,
    # )
    #
    # handler = logging.StreamHandler()
    # handler.setFormatter(formatter)
    # handler.setLevel(logging.INFO)
    #
    # root_logger.handlers = []
    # root_logger.addHandler(handler)
    # root_logger.propagate = False

    logging.getLogger('paramiko.transport').setLevel(logging.WARNING)
