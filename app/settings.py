import logging
from typing import Optional

import pysftp
import structlog
from dotenv import load_dotenv
from pydantic import BaseSettings, validator

log = structlog.getLogger(__name__)
load_dotenv()  # take environment variables from .env

log_levels = {
    'critical': logging.CRITICAL,
    'error': logging.ERROR,
    'warn': logging.WARNING,
    'warning': logging.WARNING,
    'info': logging.INFO,
    'debug': logging.DEBUG
}


class Settings(BaseSettings):
    s3_endpoint_url: str
    s3_region: str
    s3_access_key: str
    s3_access_secret: str
    s3_bucket: str
    asset_base_url: str
    app_env: str = "dev"
    db_host: str
    db_database: str
    db_user: str
    db_password: str
    db_port: int = 3306
    max_thumbnail_dim: int = 3000
    unzip_cmd: str = "/usr/bin/unzip"
    log_level: Optional[int] = logging.DEBUG
    log_renderer: Optional[str] = "json"
    sftp_known_hosts_path: Optional[str] = pysftp.helpers.known_hosts()

    app_session_secret: Optional[str] = None

    rabbitmq_url: Optional[str] = None

    auth_issuer: Optional[str] = None
    auth_audience: Optional[str] = None

    # Export integrations
    promus_sftp_host: Optional[str] = None
    promus_sftp_user: Optional[str] = None
    promus_sftp_password: Optional[str] = None
    promus_sftp_path: Optional[str] = None
    nettbutikken_graphql_url: Optional[str] = None
    nettbutikken_graphql_apikey: Optional[str] = None
    nettbutikken_graphql_folder_id: Optional[int] = None

    # Import integrations
    bokbasen_username: Optional[str] = None
    bokbasen_password: Optional[str] = None
    ingram_host: Optional[str] = None
    ingram_username: Optional[str] = None
    ingram_password: Optional[str] = None
    gardners_host: Optional[str] = None
    gardners_username: Optional[str] = None
    gardners_password: Optional[str] = None
    gardners_dvd_host: Optional[str] = None
    gardners_dvd_username: Optional[str] = None
    gardners_dvd_password: Optional[str] = None
    aja_sftp_host: Optional[str] = None
    aja_sftp_username: Optional[str] = None
    aja_sftp_password: Optional[str] = None
    aja_sftp_path: Optional[str] = None

    # Other integrations
    sentry_dsn: Optional[str] = None

    redis_url: Optional[str] = None

    # Pillow will throw PIL.Image.DecompressionBombError if it encounters a too large image, see
    # https://pillow.readthedocs.io/en/stable/releasenotes/5.0.0.html#decompression-bombs-now-raise-exceptions
    # The default limit of 178 956 970 is a bit too small for us, the largest image we have encountered so far
    # is 277 504 355 pixels (To check: `select max(width * height) from asset;`).
    MAX_IMAGE_PIXELS: int = 600000000

    @validator('log_level', pre=True, always=True)
    def set_log_level(cls, value):
        if isinstance(value, str):
            log_level = log_levels.get(value.lower())
            if log_level is None:
                raise ValueError(
                    f"Invalid LOG_LEVEL value '{value}', must be one of: {' | '.join(log_levels.keys())}")
            return log_level
        return value

    @property
    def db_url(self):
        return f"mysql+mysqldb://{self.db_user}:{self.db_password}@{self.db_host}:{self.db_port}/{self.db_database}?charset=utf8mb4"


settings = Settings()
