from datetime import datetime
from time import perf_counter
from dramatiq import ActorNotFound
from dramatiq.middleware import Middleware
from threading import local
import structlog
from app import models
from app.database import db_session


class StructlogDramatiqMiddleware(Middleware):
    """Provides pretty logging for tasks processed on the workers."""

    def __init__(self):
        self.state = local()
        self.log = structlog.get_logger(__name__)

    def before_process_message(self, broker, message):
        self.state.start = perf_counter()
        structlog.threadlocal.clear_threadlocal()
        structlog.threadlocal.bind_threadlocal(job_id=message.message_id,
                                               task=message.actor_name)
        log = self.log.bind()
        log.info('Starting task')

    def after_process_message(self, broker, message, *, result=None, exception=None):
        try:
            log = self.log.bind()
            delta = perf_counter() - self.state.start
            outcome = 'Task failed' if exception else 'Completed task'
            log.info(outcome, task_time_msecs="%.02f" % (delta * 1000))
            del self.state.start
            structlog.threadlocal.clear_threadlocal()
        except AttributeError:
            pass

    after_skip_message = after_process_message


class StoreJobStatus(Middleware):
    """Store job status in MySQL.

    If it becomes too slow, we can switch to some faster store."""

    def __init__(self, *, store_results=False):
        self.store_results = store_results

    @property
    def actor_options(self):
        return {
            "store_results",
        }

    def _store_results(self, broker, message) -> bool:
        try:
            actor = broker.get_actor(message.actor_name)
            store_results = actor.options.get("store_results", self.store_results)
            return store_results
        except ActorNotFound:
            return False

    def _db_job_status_from_message(self, db, message):
        db_job_status = db.query(models.JobStatus) \
            .filter(models.JobStatus.job_id == message.message_id) \
            .first()

        if db_job_status is None:
            db_job_status = models.JobStatus(
                job_id=message.message_id,
                job_name=message.actor_name,
            )
            db.add(db_job_status)
        return db_job_status

    def before_enqueue(self, broker, message, delay):
        # Called when a job is queued, either first time or queued for retry
        structlog.get_logger(__name__).info("before_enqueue")
        if self._store_results(broker, message):
            with db_session() as db:
                db_job_status = self._db_job_status_from_message(db, message)
                structlog.get_logger(__name__).info("before_enqueue >")
                structlog.get_logger(__name__).info("before_enqueue >>", state=db_job_status.state)
                if db_job_status.state is None:
                    db_job_status.state = 'QUEUED'
                    db_job_status.enqueued_at = datetime.now()
                db.commit()

    def before_process_message(self, broker, message):
        if self._store_results(broker, message):
            with db_session() as db:
                db_job_status = self._db_job_status_from_message(db, message)
                db_job_status.state = 'RUNNING'
                db_job_status.started_at = datetime.now()
                db_job_status.ended_at = None
                db.commit()

    def after_process_message(self, broker, message, *, result=None, exception=None):
        if self._store_results(broker, message):
            with db_session() as db:
                db_job_status = self._db_job_status_from_message(db, message)
                db_job_status.ended_at = datetime.now()
                db_job_status.retries = message.options.get('retries', 0)
                if exception is not None:
                    db_job_status.state = 'WAITING_FOR_RETRY'
                    db_job_status.result = {"error": str(exception)}
                else:
                    db_job_status.state = 'COMPLETED'
                    result = result or {}
                    db_job_status.result = result
                    try:
                        db_job_status.total_processed = sum(result["stats"].values())
                    except KeyError:
                        pass
                db.commit()

    def after_nack(self, broker, message):
        store_results = self._store_results(broker, message)
        if store_results and message.failed:
            exception = message._exception or Exception("unknown")
            with db_session() as db:
                db_job_status = self._db_job_status_from_message(db, message)
                db_job_status.state = 'FAILED'
                db_job_status.ended_at = datetime.now()
                db_job_status.result = {"error": str(exception)}
                db.commit()
