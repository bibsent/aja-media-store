from argparse import ArgumentParser
from dataclasses import asdict
from pathlib import Path

import structlog
from pydantic import BaseModel, validator
from pydantic.types import DirectoryPath

from app import models
from app.dependencies import get_s3_bucket
from app.tasks import registry
from app.tasks.util.misc import import_from_folder, HOUR

logger = structlog.get_logger(__name__)


def configure_argparse(parser: ArgumentParser):
    parser.description = """
    Import assets from a local folder. Files must have EAN numbers as filenames.
    """
    parser.add_argument("source_name", help="Name of the source/vendor the assets originate from")
    parser.add_argument("src_dir", help="Path to the folder to import assets from")
    parser.add_argument("--asset_type", help="Asset type (currently only 'cover') is supported", default="cover")
    parser.add_argument("--delete", help="Delete files after import", dest="delete_after_import", action="store_true")


class InputModel(BaseModel):
    source_name: str
    src_dir: DirectoryPath
    asset_type: str
    delete_after_import: bool

    @validator('asset_type')
    def check_asset_type(cls, v):
        if v not in {'cover'}:
            raise ValueError('invalid asset_type')
        return v


@registry.task(configure_argparse=configure_argparse, time_limit=1 * HOUR, input_model=InputModel, store_results=True)
def folder_import(args: InputModel):
    src_dir = Path(args.src_dir)
    log = logger.bind()

    status = models.ImportStatus().start()
    status.set_metadata('path', str(src_dir))

    bucket = get_s3_bucket()
    status = import_from_folder(src_dir, args.source_name, bucket, args.asset_type, status, args.delete_after_import)

    status.complete()
    log.info('Import complete', **status.stats)
    return asdict(status)
