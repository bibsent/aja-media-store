from argparse import ArgumentParser

import dateutil.parser
import sentry_sdk
import structlog

from pydantic import BaseModel, Field

from app.settings import settings
from app.tasks import registry
from app.tasks.util.ftp import import_from_ftp
from app.tasks.util.misc import HOUR, current_date

log = structlog.get_logger(__name__)


def configure_argparse(parser: ArgumentParser):
    parser.description = """
    Import book images from Gardners. By default, the importer will fetch the weekly zip file for the current week.
    Alternatively, the --date argument can be used to fetch a package from the week containing the specified date.
    """
    parser.add_argument('--date', dest='date',
                        help='Optional date (YYYY-MM-DD)')


class InputModel(BaseModel):
    date: str = Field(default_factory=current_date)


@registry.task(configure_argparse=configure_argparse, time_limit=36 * HOUR, input_model=InputModel, store_results=True)
def gardners_import(args: InputModel):
    """
    Årsaken til at `time_limit` er satt så høyt er at FTP-serveren til Gardners er på et veldig tregt nett.
    Per juni 2021 tok det nøyaktig 6 timer å laste ned 7700 MiB, noe som gir en hastighet på 0,35 MiB/s.
    De fleste av filene vi får er 1-3 GiB, men i juni kom det en fil på 15 GiB.
    """
    dt = dateutil.parser.parse(args.date)
    sentry_sdk.start_transaction(name="Import Job: Gardners", sampled=True)
    return import_from_ftp(
        source_name='gardners',
        remote_path=dt.strftime('/Books/Update/BookImages_wk%W.zip'),
        import_name=dt.strftime('%Y-%W'),
        asset_type='cover',
        ftp_host=settings.gardners_host,
        ftp_username=settings.gardners_username,
        ftp_password=settings.gardners_password
    )
