import shutil
import tempfile
from argparse import ArgumentParser
from typing import Generator, Optional

import requests
import structlog
from email.utils import formatdate
from datetime import datetime
import dateutil.parser
from time import mktime

from lxml import etree  # nosec
from dataclasses import dataclass, asdict

from pydantic import BaseModel

from app import models, schemas
from app.database import db_session
from app.dependencies import get_s3_bucket, get_http_session
from app.services import asset
from app.settings import settings
from app.tasks import registry
from app.tasks.util.misc import HOUR

logger = structlog.get_logger(__name__)


def configure_argparse(parser: ArgumentParser):
    parser.description = """
    Import images from Bokbasen. By default, the importer will fetch items created or modified since the last time
    the importer ran. Use the --date argument to fetch items from a specific date instead.
    """
    parser.add_argument('--date', dest='date',
                        help='Optional start date (YYYY-MM-DD)')


@dataclass
class BokbasenObject:
    isbn13: str
    ean: str
    url: str


class ObjectGenerator:

    def __init__(self, session: requests.Session, after=None, next=None, ):
        self.session = session
        self.after = after
        self.next = next

    def params(self):
        if self.next is not None:
            return {'next': self.next}
        return {'after': self.after}

    def get(self) -> Generator[BokbasenObject, None, None]:
        complete = False
        while not complete:
            resp = self.session.get('https://api.boknett.no/metadata/export/object', headers={
                'Date': get_rfc1123_date(),
            }, params=self.params())

            complete = 'link' not in resp.headers
            self.next = resp.headers.get('next')

            objects = etree.fromstring(resp.content)  # nosec
            for obj in objects:
                if obj.findtext('TYPE') == 'org':
                    yield BokbasenObject(
                        isbn13=obj.findtext('ISBN13'),
                        ean=obj.findtext('EAN'),
                        url=obj.findtext('REFERENCE'),
                    )


def get_rfc1123_date():
    return formatdate(timeval=mktime(datetime.now().timetuple()), localtime=False, usegmt=True)


def login(username, password):
    session = get_http_session()

    resp = session.post('https://login.boknett.no/v1/tickets', data={
        'username': username,
        'password': password,
    })
    if resp.status_code != 201:
        raise Exception('Login for Bokbasen failed')

    session.headers.update({'Authorization': 'Boknett %s' % resp.headers['boknett-tgt']})

    return session


def get_object(session, obj: BokbasenObject):
    fp = tempfile.TemporaryFile()
    with session.get(obj.url, headers={'Date': get_rfc1123_date(), 'Accept': 'application/octet-stream'},
                     stream=True) as resp:
        resp.raise_for_status()
        shutil.copyfileobj(resp.raw, fp)
    return fp


class InputModel(BaseModel):
    date: Optional[str]


@registry.task(configure_argparse=configure_argparse, time_limit=1 * HOUR, input_model=InputModel, store_results=True)
def bokbasen_import(args: InputModel):
    source_name = 'bokbasen'
    status = models.ImportStatus().start()
    log = logger.bind()

    if settings.bokbasen_username is None or settings.bokbasen_password is None:
        raise Exception('Username or password not specified')

    with db_session() as db:
        job_state = db.query(models.JobState)\
            .filter(models.JobState.job_name == source_name)\
            .first()

        if args.date is not None:
            status.set_metadata("date", args.date)
            dt = dateutil.parser.parse(args.date)
            params = {'after': dt.strftime('%Y%m%d%H%M%S')}
        elif job_state is not None and 'cursor' in job_state.state:
            status.set_metadata("cursor", job_state.state['cursor'])
            params = {'next': job_state.state['cursor']}
        else:
            raise Exception('Need either a date or a cursor to import from Bokbasen')

        session = login(settings.bokbasen_username, settings.bokbasen_password)
        log.debug('Bokbasen login OK')

        objs = ObjectGenerator(session, **params)
        bucket = get_s3_bucket()

        for obj in objs.get():
            fp = get_object(session, obj)
            res = asset.store(db, bucket, schemas.AssetIn(
                file=fp,
                asset_type='cover',
                ean=obj.ean,
                source=source_name
            ))
            status.incr(res['status'])

        job_state = db.query(models.JobState)\
            .filter(models.JobState.job_name == source_name)\
            .first()
        if job_state is None:
            job_state = models.JobState(job_name=source_name, state={})
            db.add(job_state)
        job_state.state['cursor'] = objs.next
        db.commit()

        status.complete()
        log.info('Import complete', **status.stats)
        return asdict(status)
