from argparse import ArgumentParser
from datetime import datetime

import structlog

from app.services import asset
from python_graphql_client import GraphqlClient

from app import models, event
from app.database import db_session
from app.settings import settings
from app.tasks import registry
from app.tasks.util.misc import get_asset_url
from app.tasks.util.schemas import SingleAssetExportArguments

logger = structlog.get_logger(__name__)

target_name = "nettbutikken"


def configure_argparse(parser: ArgumentParser):
    parser.description = """
    Export a single asset to Nettbutikken.
    """
    parser.add_argument('--asset_id', type=str, help='Lookup asset internal ID')
    parser.add_argument('--asset_type', type=str, help='Asset type')
    parser.add_argument('--ean', type=str, help='Lookup asset by EAN/ISBN')
    parser.add_argument('--product_uuid', type=str, help='Lookup asset by product UUID')


@registry.task(configure_argparse=configure_argparse, input_model=SingleAssetExportArguments)
def nettbutikken_export(args: SingleAssetExportArguments):
    log = logger.bind()

    with db_session() as db:
        query = asset.find(db, asset_id=args.asset_id, asset_type=args.asset_type, product_uuid=args.product_uuid, ean=args.ean)
        db_asset = query.first()

        if db_asset is None:
            log.error("Nettbutikken Export: No such asset")
            return

        client = GraphqlClient(endpoint=settings.nettbutikken_graphql_url,
                               params={"apikey": settings.nettbutikken_graphql_apikey},
                               headers={"User-Agent": "Aja"})

        data = client.execute(
            query="""
                mutation createImage($key: String!, $parentId: Int!, $imageUrl: String!) {
                  createImageUpdate(key: $key, parentId: $parentId, input: {SKU: $key, ImageUrl: $imageUrl}) {
                    output {
                      id
                    }
                    success
                    message
                  }
                }
            """,
            variables={
                "key": db_asset.product.ean,
                "imageUrl": get_asset_url(db_asset, "original.jpg"),
                "parentId": settings.nettbutikken_graphql_folder_id,
            }
        )

        db.add(models.ExportStatus(target=target_name, product_id=db_asset.product.id,
                                   asset_id=db_asset.id, date=datetime.now(),
                                   extra={"response": data}))
        db.commit()

        result = {
            "target": target_name,
            "ean": db_asset.product.ean,
            "uuid": db_asset.product.uuid,
            "asset_version": db_asset.version,
            "response": data,
        }
        log.info("Exported asset", **result)


def is_configured():
    if settings.nettbutikken_graphql_url is None or settings.nettbutikken_graphql_apikey is None:
        return False
    return True


def enable():
    if not is_configured():
        logger.warning("Integration not enabled: %s" % __name__)
        return

    def cb(db_asset):
        nettbutikken_export.send(asset_id=db_asset.id)

    event.listen(event.ASSET_CREATED, cb)
    logger.info("Integration enabled: %s" % __name__)
