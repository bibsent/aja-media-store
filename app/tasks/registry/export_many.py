import datetime
from argparse import ArgumentParser

import structlog

from app import models
from app.database import db_session
from app.tasks import registry
from app.tasks.util.misc import HOUR
from app.tasks.util.schemas import MassAssetExportArguments

logger = structlog.get_logger()


def configure_argparse_many(parser: ArgumentParser):
    parser.description = """Export products not yet exported to a given target."""
    parser.add_argument('--target', type=str, help='Target name')
    parser.add_argument('--start', type=str, help='Products created from this date (inclusive)')
    parser.add_argument('--end', type=str, help='Products created until this date (inclusive)')


@registry.task(configure_argparse=configure_argparse_many,
               input_model=MassAssetExportArguments,
               time_limit=1 * HOUR)
def export_many(args: MassAssetExportArguments):
    export_task = registry.get(args.target + '_export')

    log = logger.bind()
    log.info("Started batch export job")
    with db_session() as db:

        # Find all products that have not been exported yet
        query = db.query(models.Product) \
            .outerjoin(models.Product.export_status.and_(models.ExportStatus.target == args.target))\
            .filter(models.ExportStatus.id.is_(None))

        if args.start:
            query = query.filter(models.Product.created >= datetime.datetime.combine(args.start, datetime.datetime.min.time()))

        if args.end:
            query = query.filter(models.Product.created <= datetime.datetime.combine(args.end, datetime.datetime.max.time()))

        cnt = 0
        for product in query:
            latest_asset = product.assets.get("cover")
            export_task.send(asset_id=latest_asset.id)
            cnt += 1

    log.info("Queued %d assets for export to target '%s'" % (cnt, args.target))
