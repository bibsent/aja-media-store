# Source: https://github.com/Bogdanp/dramatiq/pull/329
import importlib
import pkgutil
import sys
from argparse import ArgumentParser
from typing import Optional, Callable, Dict
import structlog
from dramatiq.actor import Actor, _queue_name_re
from pydantic import BaseModel
from sqlalchemy.orm import Session

from app import models

ConfigureFn = Callable[[ArgumentParser], None]
CanExportFn = Callable[[Session, models.Asset], bool]

log = structlog.get_logger()


def import_submodules(package_name):
    """Import all submodules of a module, recursively"""
    package = sys.modules[package_name]
    for loader, name, is_pkg in pkgutil.walk_packages(package.__path__):
        yield importlib.import_module(package_name + '.' + name)
        log.debug('Loaded task module: %s' % package_name + '.' + name)


class MediaStoreActor(Actor):
    """
    Extension to Actor that adds a `configure_argparse` method that is used by our CLI.
    """
    def __init__(self, fn, *,
                 configure_argparse: Optional[ConfigureFn] = None,
                 input_model: Optional[BaseModel] = None,
                 **kwargs):
        super().__init__(fn, **kwargs)
        if configure_argparse is not None:
            self.configure_argparse = configure_argparse
        else:
            self.configure_argparse: ConfigureFn = lambda parser: None
        if input_model is not None:
            self.input_model = input_model
        else:
            self.input_model = BaseModel
        # log.debug("Created dramatiq actor: %s" % self.actor_name)

    def __call__(self, *args, **kwargs):
        model = self.input_model(**kwargs)
        return super().__call__(model)


class TaskRegistry:
    """
    A Registry allows defining a collection of Actors not directly bound to a Broker.
    This allows your code to declare Actors before configuring a Broker.
    """
    def __init__(self):
        self.actors = {}
        self.broker = None

    def load_all(self, enable=True):
        for module in import_submodules('app.tasks.registry'):
            if enable and 'enable' in dir(module):
                module.enable()
        return self

    def task(
        self,
        fn=None,
        *,
        actor_class=MediaStoreActor,
        actor_name=None,
        queue_name="media_store_default",
        priority=0,
        configure_argparse=None,
        input_model=None,
        **options
    ):
        """
        Mimics the `dramatiq.actor.actor` decorator, but delays the registration and actor options check
        if a broker has not yet been defined, instead of creating a new broker.
        """
        if 'time_limit' in options:
            options['time_limit'] = options['time_limit'] * 1000  # millisekunder -> sekunder

        def decorator(fn):
            if not _queue_name_re.fullmatch(queue_name):
                raise ValueError(
                    "Queue names must start with a letter or an underscore followed "
                    "by any number of letters, digits, dashes or underscores."
                )

            return actor_class(
                fn,
                actor_name=actor_name or fn.__name__,
                queue_name=queue_name,
                priority=priority,
                broker=self,  # Note: passing self as broker
                configure_argparse=configure_argparse,
                input_model=input_model,
                options=options,
            )

        if fn is None:
            return decorator
        return decorator(fn)

    def __getattr__(self, name):
        # Proxy everything else to our Broker, if set.
        return getattr(self.broker, name)

    def declare_actor(self, actor):
        """
        Intercept when Actor class tries to register itself.
        """
        if not self.broker:
            self.actors[actor.actor_name] = actor
            return

        # log.debug("Register actor %s with broker" % actor.actor_name)
        invalid_options = set(actor.options) - self.broker.actor_options
        if invalid_options:
            invalid_options_list = ", ".join(invalid_options)
            raise ValueError((
                "Actor %s specified the following options which are not "
                "supported by this broker: %s. Did you forget to add a "
                "middleware to your Broker?"
            ) % (actor.actor_name, invalid_options_list))

        self.broker.declare_actor(actor)

    def bind_broker(self, broker):
        self.broker = broker
        for actor_name, actor in self.actors.items():
            self.declare_actor(actor)

    def all(self) -> Dict[str, MediaStoreActor]:
        if self.broker:
            return self.broker.actors
        return self.actors

    def has(self, key: str) -> bool:
        return key in self.all()

    def get(self, key: str) -> MediaStoreActor:
        return self.all()[key]
