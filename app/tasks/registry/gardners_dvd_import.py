from argparse import ArgumentParser

import structlog
import dateutil.parser
from pydantic import Field

from pydantic import BaseModel

from app.settings import settings
from app.tasks import registry
from app.tasks.util.ftp import import_from_ftp
from app.tasks.util.misc import HOUR, current_date

log = structlog.get_logger(__name__)


def configure_argparse(parser: ArgumentParser):
    parser.description = """
    Import DVD images from Gardners. By default, the importer will fetch the weekly zip file for the current week.
    Alternatively, the --date argument can be used to fetch a package from the week containing the specified date.
    """
    parser.add_argument('--date', dest='date',
                        help='Optional date (YYYY-MM-DD)')


class InputModel(BaseModel):
    date: str = Field(default_factory=current_date)


@registry.task(configure_argparse=configure_argparse, time_limit=1 * HOUR, input_model=InputModel, store_results=True)
def gardners_dvd_import(args: InputModel):
    dt = dateutil.parser.parse(args.date)
    return import_from_ftp(
        source_name='gardners_dvd',
        remote_path=dt.strftime('/DVD/Update/Week%W.zip'),
        import_name=dt.strftime('%Y-%W'),
        asset_type='cover',
        ftp_host=settings.gardners_dvd_host,
        ftp_username=settings.gardners_dvd_username,
        ftp_password=settings.gardners_dvd_password
    )
