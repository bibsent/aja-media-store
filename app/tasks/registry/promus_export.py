import os
import re
from argparse import ArgumentParser
from datetime import datetime
from io import BytesIO
from typing import Optional

import structlog
from sqlalchemy.orm import Session

from app.services import asset
from lxml import etree as ET  # nosec
from lxml.builder import ElementMaker  # nosec

from app import models, event
from app.settings import settings
from app.database import db_session
from app.tasks import registry
from app.tasks.util.misc import get_asset_url
from app.tasks.util.schemas import SingleAssetExportArguments
from app.tasks.util.sftp import sftp_session

logger = structlog.get_logger(__name__)

target_name = "promus"


def generate_xml(asset: models.Asset) -> bytes:
    url_original = get_asset_url(asset, "original.jpg")
    url_thumbnail = get_asset_url(asset, "thumbnail.jpg")

    E = ElementMaker()
    return ET.tostring(
        E.root(
            E.item(
                E.uuid(asset.product.uuid),
                E.id(asset.product.ean),
                E.cover_url_original(url_original),
                E.cover_url_thumbnail(url_thumbnail),
            ),
        ),
        pretty_print=True,
        xml_declaration=True,
        encoding="UTF-8"
    )


def reason_to_not_export(db: Session, db_asset: models.Asset, force: bool) -> Optional[str]:
    """Is there a reason that we should NOT export this asset to Promus?"""

    # 1. If it's not a cover, Promus is not interested.
    if db_asset.asset_type != "cover":
        return "Not a cover"

    # 2. The cover URL does not change if we get a new version of the image, so if the URL has already been
    #    exported to Promus, there's no need to export it again.
    export_status = db.query(models.ExportStatus)\
        .filter(models.ExportStatus.target == target_name)\
        .filter(models.ExportStatus.product_id == db_asset.product_id)\
        .first()
    if export_status is not None and not force:
        return "Already exported"

    # 4. If the asset is from Bokbasen, we're only allowed to export it for a few publishers.
    if db_asset.source == "bokbasen":
        bokbasen_whitelist = [
            # Cappelen Damm
            "9788202",
            "9788204",
            # Aschehoug
            "9788203",
            # Oktober
            "97882495",
            "978827094",
            # Universitetsforlaget
            "9788200",
            "9788215",
            "97882518",
        ]
        bokbasen_whitelist_matcher = re.compile(r"^(?:%s)" % "|".join(bokbasen_whitelist))

        if not bokbasen_whitelist_matcher.match(db_asset.product.ean):
            return "Not allowed to export this Bokbasen asset"


def configure_argparse_single(parser: ArgumentParser):
    parser.description = """
    Export a single asset to Promus.
    """
    parser.add_argument('--asset_id', type=str, help='Lookup asset internal ID')
    parser.add_argument('--asset_type', type=str, help='Asset type')
    parser.add_argument('--ean', type=str, help='Lookup asset by EAN/ISBN')
    parser.add_argument('--product_uuid', type=str, help='Lookup asset by product UUID')
    parser.add_argument('--force', action='store_true', help='Export even if already exported')


@registry.task(configure_argparse=configure_argparse_single,
               input_model=SingleAssetExportArguments)
def promus_export(args: SingleAssetExportArguments):
    log = logger.bind()

    with db_session() as db:
        query = asset.find(db, asset_id=args.asset_id, asset_type=args.asset_type, product_uuid=args.product_uuid, ean=args.ean)
        db_asset = query.first()

        if db_asset is None:
            log.error("Promus Export: No such asset")
            return

        if reason := reason_to_not_export(db, db_asset, args.force):
            log.info("Promus Export: Cannot export asset: %s" % reason, ean=db_asset.product.ean)
            return

        stream = BytesIO(generate_xml(db_asset))
        path = os.path.join(settings.promus_sftp_path, "%s.xml" % db_asset.product.ean)
        with sftp_session(settings.promus_sftp_host, settings.promus_sftp_user, settings.promus_sftp_password) as sftp:
            sftp.putfo(stream, path)

        db.add(models.ExportStatus(target=target_name, product_id=db_asset.product.id,
                                   asset_id=db_asset.id, date=datetime.now()))
        db.commit()

        log.info("Exported XML to Promus", ean=db_asset.product.ean,
                 uuid=db_asset.product.uuid, asset_version=db_asset.version)


def is_configured():
    if settings.promus_sftp_host is None or settings.promus_sftp_user is None or settings.promus_sftp_password is None:
        return False
    return True


def enable():
    if not is_configured():
        logger.warning("Integration not enabled: %s" % __name__)
        return

    with sftp_session(settings.promus_sftp_host, settings.promus_sftp_user, settings.promus_sftp_password) as sftp:
        try:
            sftp.stat(settings.promus_sftp_path)
        except FileNotFoundError:
            raise Exception("settings.promus_sftp_path does not exist: %s" % settings.promus_sftp_path)

    def cb(db_asset):
        logger.debug("Received ASSET_CREATED event")
        with db_session() as db:
            if reason := reason_to_not_export(db, db_asset, False):
                logger.info("Skipping export to %s" % target_name, reason=reason,
                            ean=db_asset.product.ean, uuid=db_asset.product.uuid, asset_version=db_asset.version)
                return
            promus_export.send(asset_id=db_asset.id)
    event.listen(event.ASSET_CREATED, cb)
    logger.info("Integration enabled: %s" % __name__)
