from datetime import datetime

import structlog
from pydantic import BaseModel
from sqlalchemy import text

from app import models
from app.database import db_session
from app.tasks import registry
from app.tasks.util.misc import HOUR

logger = structlog.get_logger(__name__)


@registry.task(time_limit=1 * HOUR)
def update_stats(args: BaseModel):
    log = logger.bind()
    data = {"by_isbn": {}, "by_source": {}}

    with db_session() as db:
        res = db.execute(text("select count(*) from product")).fetchone()
        data["total"] = res[0]

        # Bookland (978)
        res = db.execute(text("select count(*) from product where ean like '978%'")).fetchone()
        data["by_isbn"]["978"] = res[0]

        # Norge (978-82)
        res = db.execute(text("select count(*) from product where ean like '97882%'")).fetchone()
        data["by_isbn"]["97882"] = res[0]

        # By source
        res = db.execute(text("""
        SELECT source, count(*) FROM
        (
            SELECT
              source,
              row_number() over (partition by product_id
                                 order by created desc) as asset_rank
            FROM asset
        ) t
        WHERE asset_rank = 1
        GROUP BY source
        """)).fetchall()
        for row in res:
            data["by_source"][row[0]] = row[1]

        db.add(models.Stats(
            date=datetime.utcnow(),
            data=data
        ))
        db.commit()

    log.info('Stats updated')
