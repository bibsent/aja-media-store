from argparse import ArgumentParser

import structlog
from datetime import datetime, timedelta
import dateutil.parser
from pydantic import Field, BaseModel

from app.settings import settings
from app.tasks import registry
from app.tasks.util.ftp import import_from_ftp
from app.tasks.util.misc import HOUR

log = structlog.get_logger(__name__)


def configure_argparse(parser: ArgumentParser):
    parser.description = """
    Import images from Ingram. By default, the importer will fetch the weekly zip file published last Sunday.
    Alternatively, the --date argument can be used to fetch a package from the week containing the specified date.
    """
    parser.add_argument('--date', dest='date',
                        help='Optional date (YYYY-MM-DD)')


def last_sunday() -> str:
    return (datetime.now() - timedelta(days=(datetime.now().isoweekday() % 7))).strftime('%Y-%m-%d')


class InputModel(BaseModel):
    date: str = Field(default_factory=last_sunday)


@registry.task(configure_argparse=configure_argparse, time_limit=24 * HOUR, input_model=InputModel, store_results=True)
def ingram_import(args: InputModel):
    dt = dateutil.parser.parse(args.date)
    return import_from_ftp(
        source_name='ingram',
        remote_path=dt.strftime('/Imageswk/J400w/%m%d%yj400-13_file*.zip'),
        import_name=dt.strftime('%Y-%W'),
        asset_type='cover',
        ftp_host=settings.ingram_host,
        ftp_username=settings.ingram_username,
        ftp_password=settings.ingram_password,
    )
