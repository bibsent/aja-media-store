import os
import re
from argparse import ArgumentParser
from dataclasses import asdict
from io import BytesIO

import structlog
from pydantic import BaseModel

from app import models, schemas
from app.database import db_session
from app.dependencies import get_s3_bucket
from app.services import asset
from app.settings import settings
from app.tasks import registry
from app.tasks.util.misc import HOUR
from app.tasks.util.sftp import sftp_session

logger = structlog.get_logger(__name__)


def configure_argparse(parser: ArgumentParser):
    parser.description = """
    Import assets from Aja SFTP server. Files must have ISBN/EAN numbers as filenames.
    """
    parser.add_argument("--no-delete", help="Don't delete files after import", dest="delete_after_import",
                        action="store_false")


class InputModel(BaseModel):
    delete_after_import: bool = True


@registry.task(configure_argparse=configure_argparse, time_limit=1 * HOUR, input_model=InputModel, store_results=True)
def aja_sftp_import(args: InputModel):
    log = logger.bind()
    source_name = 'aja_sftp'
    bucket = get_s3_bucket()
    status = models.ImportStatus().start()
    with db_session() as db:
        with sftp_session(settings.aja_sftp_host, settings.aja_sftp_username, settings.aja_sftp_password) as conn:
            with conn.cd(settings.aja_sftp_path):
                for filename in conn.listdir():
                    match = re.match('([0-9]{13}).[a-z]{3,4}', filename)
                    if match:
                        ean = match.group(1)
                        remote_path = os.path.join(settings.aja_sftp_path, filename)
                        log.info('Importing file', path=remote_path)
                        flo = BytesIO()
                        try:
                            conn.getfo(remote_path, flo)
                            flo.seek(0)
                            res = asset.store(db, bucket, schemas.AssetIn(
                                file=flo,
                                asset_type='cover',
                                ean=ean,
                                source=source_name
                            ))
                            status.incr(res['status'])
                            if args.delete_after_import:
                                conn.unlink(remote_path)
                        except IOError as err:
                            log.warning('SFTP IO error', details=err.message, path=str(remote_path))

        status.complete()
        if sum(status.stats.values()) > 0:
            log.info('Import complete', **status.stats)
        else:
            log.info('Nothing to import')

    return asdict(status)
