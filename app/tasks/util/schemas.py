from typing import Optional

from pydantic import BaseModel, root_validator, validator
from pydantic.schema import date

from app.tasks import registry


class SingleAssetExportArguments(BaseModel):
    asset_id: Optional[int]
    asset_type: Optional[str]
    ean: Optional[str]
    product_uuid: Optional[str]
    force: bool = False

    class Config:
        extra = "forbid"

    @root_validator(pre=False, skip_on_failure=True)
    def valid_job_arguments(cls, values):
        if values.get("asset_id") and values.get("asset_type"):
            raise ValueError("asset_id cannot be combined with asset_type")
        nargs = 0
        if values.get("asset_id"):
            nargs += 1
        if values.get("ean"):
            nargs += 1
        if values.get("product_uuid"):
            nargs += 1
        if nargs == 0:
            raise ValueError("One of asset_id, ean and product_uuid must be specified")
        if nargs > 1:
            raise ValueError("Only one of asset_id, ean and product_uuid must be specified")
        return values


class MassAssetExportArguments(BaseModel):
    target: str
    start: Optional[date]
    end: Optional[date]

    @validator('target')
    def validate_target_name(cls, v):
        print('VALIDN', list(registry.all().keys()))
        valid_names = {k.replace('_export', '') for k in registry.all().keys() if '_export' in k}
        if v not in valid_names:
            raise ValueError('Invalid export target: ' + str(v))
        return v
