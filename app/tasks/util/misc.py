import os
import re
import subprocess  # nosec
from datetime import datetime
from pathlib import Path
from typing import Optional

import dateutil.parser
import structlog

from app import models, schemas
from app.database import db_session
from app.services import asset
from app.settings import settings

logger = structlog.get_logger(__name__)

HOUR = 3600000  # milliseconds


def get_asset_url(asset: models.Asset, variant: str):
    return os.path.join(
        settings.asset_base_url,
        asset.product.uuid,
        asset.asset_type,
        variant
    )


def parse_date(value: Optional[str] = None) -> Optional[datetime]:
    if value is not None:
        input_date = dateutil.parser.parse(value)
        return input_date


def unzip(log, zip_file: Path):
    try:
        subprocess.run(  # nosec
            [settings.unzip_cmd, '-n', '-qq', zip_file.name],
            cwd=zip_file.parent,
            capture_output=True,
            check=True
        )
        log.info('Unzip successful', file=str(zip_file))
    except subprocess.CalledProcessError as err:
        log.error('Unzip failed', file=str(zip_file), detail=str(err) + ' ' + str(err.stderr))
        # Probably corrupt/incomplete download. Unlink file so it will be re-downloaded on next retry
        zip_file.unlink()
        raise err


def import_from_folder(import_dir: Path, source_name: str, bucket, asset_type: str, status: models.ImportStatus,
                       delete_after_import: bool):
    with db_session() as db:
        for file in import_dir.iterdir():
            match = re.match('([0-9]{13}).(jpg|jpeg|jfif|png|webp)', file.name, re.IGNORECASE)
            if match:
                ean = match.group(1)
                with file.open('rb') as fp:
                    res = asset.store(db, bucket, schemas.AssetIn(
                        file=fp,
                        asset_type=asset_type,
                        ean=ean,
                        source=source_name
                    ))
                status.incr(res['status'])
                if delete_after_import:
                    file.unlink()
        return status


def current_date() -> str:
    return datetime.now().date().strftime('%Y-%m-%d')
