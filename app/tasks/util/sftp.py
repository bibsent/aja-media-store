import contextlib
from hashlib import md5
from typing import ContextManager

import pysftp
import structlog
from paramiko.pkey import PKey

from app.settings import settings

log = structlog.get_logger(__name__)


def format_pubkey(value: PKey):
    value = md5(value.asbytes()).hexdigest()  # nosec
    return ':'.join([value[i:i + 2] for i in range(0, len(value), 2)])


@contextlib.contextmanager
def sftp_session(host, username, password, port=22) -> ContextManager[pysftp.Connection]:
    if username is None or password is None:
        raise Exception('Username or password for SFTP server not specified')

    # The first time we connect to a new host, we will accept any host key and store it to the file
    # specified in `settings.known_hosts_path`, so that we can validate it next time we connect to the same host.
    # Source: https://stackoverflow.com/a/53669608/489916

    known_hosts_path = settings.sftp_known_hosts_path
    cnopts = pysftp.CnOpts(knownhosts=known_hosts_path)
    hostkeys = None
    if cnopts.hostkeys.lookup(host) is None:
        log.info("First time connecting to SFTP host - will accept any host key", sftp_host=host)
        hostkeys = cnopts.hostkeys
        cnopts.hostkeys = None

    sftp = pysftp.Connection(host, username=username, password=password, port=port, cnopts=cnopts)

    if hostkeys is not None:
        log.info("Connected to new host, storing its hostkey", sftp_host=host,
                 sftp_hostkey=format_pubkey(sftp.remote_server_key))
        hostkeys.add(host, sftp.remote_server_key.get_name(), sftp.remote_server_key)
        hostkeys.save(known_hosts_path)
    else:
        log.info("Connected to known host", sftp_host=host,
                 sftp_hostkey=format_pubkey(sftp.remote_server_key))
    try:
        yield sftp
    finally:
        sftp.close()
