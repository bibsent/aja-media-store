import fnmatch
import os
import tempfile
from dataclasses import asdict
from pathlib import Path

import sentry_sdk
import structlog
from ftpretty import ftpretty

from app import models
from app.dependencies import get_s3_bucket
from app.tasks.util.misc import unzip, import_from_folder

logger = structlog.get_logger(__name__)


def ftp_find_source_files(conn, filename):
    basename_pattern = os.path.basename(filename)
    remote_dir = os.path.dirname(filename)
    for res in conn.list(remote_dir, extra=True):
        if fnmatch.fnmatch(res['name'], basename_pattern):
            yield {
                'path': os.path.join(remote_dir, res['name']),
                'size': res['size'],
            }


def ftp_download(log, conn, source_file_pattern: str, target_dir: Path):
    for source_file in list(ftp_find_source_files(conn, source_file_pattern)):
        remote_size = source_file['size']
        source_path = source_file['path']
        name = os.path.basename(source_path)
        target_path = target_dir.joinpath(name)
        if os.path.exists(target_path):
            log.info('Skipping %s, already exists' % target_path)
            # TODO: Validate that filesize is correct?
        else:
            log.info('Starting download from FTP server', filename=name, target_path=str(target_path), size=remote_size)
            with sentry_sdk.start_span(op="ftp_get", description="GET %s" % name):
                conn.get(source_path, str(target_path))
            log.info('Completed download from FTP server', filename=name)

        yield target_path


def import_from_ftp(source_name: str, remote_path: str, import_name: str, asset_type: str,
                    ftp_host: str, ftp_username: str, ftp_password: str):
    """
    Import zipped files from ftp server
    """
    log = logger.bind()

    # Setup
    status = models.ImportStatus().start()
    status.set_metadata('import_name', import_name)
    status.set_metadata('filename', os.path.basename(remote_path))
    temp_dir = Path(tempfile.gettempdir()).joinpath('media_store_%s_%s' % (source_name, import_name))
    temp_dir.mkdir(parents=True, exist_ok=True)

    # Download
    if ftp_host is None or ftp_username is None or ftp_password is None:
        raise Exception('FTP host or credentials not specified')
    log.debug(f"Connecting to {ftp_host}")
    conn = ftpretty(ftp_host, ftp_username, ftp_password,
                    timeout=30)  # Note: A transfer will not be cancelled if it takes longer than the timeout
    downloaded = list(ftp_download(log, conn, remote_path, temp_dir))
    if len(downloaded) == 0:
        raise Exception('Could not locate any files on FTP server matching the pattern: %s' % remote_path)
    status.time('download')

    # Unzip
    for file in downloaded:
        unzip(log, file)
    status.time('unzip')

    # Import
    bucket = get_s3_bucket()
    status = import_from_folder(temp_dir, source_name, bucket, asset_type, status, True)
    status.time('import')

    # Finalize
    log.info('Import complete', **status.stats)
    status.complete()
    return asdict(status)
