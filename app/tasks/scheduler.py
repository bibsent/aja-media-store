import signal
import sys
from typing import Callable

import dramatiq
import structlog
from apscheduler.schedulers.blocking import BlockingScheduler
from apscheduler.triggers.interval import IntervalTrigger
from apscheduler.triggers.cron import CronTrigger

from . import registry, configure_task_broker
from ..logging import configure_logging
from ..sentry import configure_sentry
from ..settings import settings

configure_logging()
logger = structlog.get_logger()
configure_sentry()
broker = configure_task_broker()


def schedule_job(job_name: str) -> Callable[[], None]:
    task = registry.get(job_name)

    def send():
        task.send()
        # Fixes "Stream connection lost" problem
        # https://github.com/Bogdanp/django_dramatiq/issues/44#issuecomment-511375731
        dramatiq.get_broker().connection.close()

    return send


def run():
    log = logger.bind()

    scheduler = BlockingScheduler()

    # Aja SFTP import every five minutes
    scheduler.add_job(
        schedule_job('aja_sftp_import'),
        IntervalTrigger(minutes=5),
        name='aja_sftp_import'
    )

    if settings.app_env == "prod":

        # Bokbasen import every hour
        scheduler.add_job(
            schedule_job('bokbasen_import'),
            IntervalTrigger(hours=1),
            name='bokbasen_import'
        )

        # Ingram import every monday morning
        scheduler.add_job(
            schedule_job('ingram_import'),
            CronTrigger(day_of_week='sun', hour=23),  # Note: UTC time
            name='ingram_import'
        )

        # Gardners DVD import every monday evening
        scheduler.add_job(
            schedule_job('gardners_import'),
            CronTrigger(day_of_week='mon', hour=22),  # Note: UTC time
            name='gardners_import'
        )

        # Gardners import every monday evening
        scheduler.add_job(
            schedule_job('gardners_dvd_import'),
            CronTrigger(day_of_week='mon', hour=23),  # Note: UTC time
            name='gardners_dvd_import'
        )

    # Update stats daily
    scheduler.add_job(
        schedule_job('update_stats'),
        CronTrigger(hour=0),
        name='update_stats'
    )

    def shutdown(signum, frame):
        log.info('Stopping scheduler')
        scheduler.shutdown()
        sys.exit(0)

    signal.signal(signal.SIGINT, shutdown)
    signal.signal(signal.SIGTERM, shutdown)

    log.info("Starting scheduler")
    scheduler.start()
