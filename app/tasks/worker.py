from . import configure_task_broker
from .middleware import StructlogDramatiqMiddleware, StoreJobStatus
from ..logging import configure_logging
from ..sentry import configure_sentry

configure_logging()
configure_sentry()
broker = configure_task_broker()
broker.add_middleware(StructlogDramatiqMiddleware())
broker.add_middleware(StoreJobStatus())
