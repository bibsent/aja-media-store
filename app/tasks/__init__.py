import dramatiq
import structlog
from dramatiq.brokers.rabbitmq import RabbitmqBroker

from app.tasks.middleware import StoreJobStatus
from app.tasks.registry import TaskRegistry
from app.settings import settings

registry = TaskRegistry()

logger = structlog.get_logger(__name__)


def configure_task_broker(load_all_tasks: bool = True):
    if load_all_tasks:
        registry.load_all()
    broker = RabbitmqBroker(url=settings.rabbitmq_url)
    broker.add_middleware(StoreJobStatus())
    dramatiq.set_broker(broker)
    registry.bind_broker(broker)
    return broker
