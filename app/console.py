import argparse
from pprint import pprint

import botocore
import structlog

from app.dependencies import get_s3_bucket, get_s3_client
from app import tasks
from app.sentry import configure_sentry

log = structlog.get_logger(__name__)


def get_lifecycle_policy(args):
    bucket = get_s3_bucket()
    print('Versioning:', bucket.Versioning().status)
    print('Lifecycle rules:')
    config = bucket.LifecycleConfiguration()
    pprint(config.rules)


def configure_s3_bucket_cmd(args):
    bucket = get_s3_bucket()
    client = get_s3_client()

    try:
        client.head_bucket(Bucket=bucket.name)
    except botocore.exceptions.ClientError as e:
        error_code = int(e.response['Error']['Code'])
        if error_code == 403:
            raise Exception("No access to bucket: %s" % bucket.name)
        elif error_code == 404:
            bucket.create()
            bucket.wait_until_exists()
            print("Created bucket: %s" % bucket.name)

    bucket.Versioning().enable()
    config = bucket.LifecycleConfiguration()
    config.put(
        LifecycleConfiguration={
            'Rules': [
                {
                    'ID': 'delete-prior-originals',
                    'Status': 'Enabled',
                    'NoncurrentVersionExpiration': {
                        'NoncurrentDays': 365,
                    },
                    'Filter': {
                        'Prefix': 'originals/',
                    },
                },
                {
                    'ID': 'delete-prior-variants',
                    'Status': 'Enabled',
                    'NoncurrentVersionExpiration': {
                        'NoncurrentDays': 7,
                    },
                    'Filter': {
                        'Prefix': 'variants/',
                    },
                },
            ]
        },
    )
    print("Lifecycle configuration updated for bucket: %s" % bucket.name)
    print(config.rules)
    return bucket


def run_task_cmd(args):
    task = tasks.registry.get(args.run_task)
    kwargs = {
        k: v for k, v in vars(args).items()
        if k not in ['cmd', 'run_task', 'no_queue'] and v is not None
    }
    model = task.input_model(**kwargs)  # Validate arguments here, since task.send does not
    if args.no_queue:
        task(**model.__dict__)
    else:
        task.send(**model.__dict__)
        log.info(f"Task '{task.actor_name}' added to queue '{task.queue_name}'")


def run():
    parser = argparse.ArgumentParser(description='aja-media-store cli')
    subparsers = parser.add_subparsers(dest='cmd')
    subparsers.required = True

    subparsers.add_parser("configure_s3_bucket")
    subparsers.add_parser("get_lifecycle_policy")

    run_parser = subparsers.add_parser("run_task")
    run_parsers = run_parser.add_subparsers(dest='run_task')
    run_parsers.required = True

    configure_sentry()
    tasks.configure_task_broker()

    for name, task in tasks.registry.all().items():
        run_parser = run_parsers.add_parser(name)

        # Add arguments specific for this runer
        task.configure_argparse(run_parser)

        # Add generic arguments that applies to all
        run_parser.add_argument('--no-queue', dest='no_queue', action='store_true',
                                help='Run import directly instead of queuing it')

    args = parser.parse_args()

    # Enable Sentry as well here?
    globals()[args.cmd + '_cmd'](args)
