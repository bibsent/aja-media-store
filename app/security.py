import asyncio
from json import JSONDecodeError
from socket import AF_INET
from typing import Optional, Any, Dict

import aiohttp
import structlog
from fastapi import HTTPException, status, Depends
from fastapi.security import HTTPBearer, SecurityScopes
from pyjwt_key_fetcher import AsyncKeyFetcher
from pyjwt_key_fetcher.errors import JWTKeyFetcherError, JWTHTTPFetchError
from pyjwt_key_fetcher.http_client import HTTPClient
from sqlalchemy.orm import Session
from starlette.requests import Request
import jwt

from app import schemas, models
from app.settings import settings

log = structlog.get_logger(__name__)

SIZE_POOL_AIOHTTP = 100


class SingletonAiohttp:
    """
    Source: https://github.com/raphaelauv/fastAPI-aiohttp-example/blob/master/src/fastAPI_aiohttp/fastAPI.py
    """
    sem: Optional[asyncio.Semaphore] = None
    aiohttp_client: Optional[aiohttp.ClientSession] = None

    @classmethod
    def get_aiohttp_client(cls) -> aiohttp.ClientSession:
        if cls.aiohttp_client is None:
            timeout = aiohttp.ClientTimeout(total=10)
            connector = aiohttp.TCPConnector(family=AF_INET, limit_per_host=SIZE_POOL_AIOHTTP)
            cls.aiohttp_client = aiohttp.ClientSession(timeout=timeout, connector=connector)

        return cls.aiohttp_client

    @classmethod
    async def close_aiohttp_client(cls) -> None:
        if cls.aiohttp_client:
            await cls.aiohttp_client.close()
            cls.aiohttp_client = None

    @classmethod
    def get(cls, url: str) -> Any:
        return cls.get_aiohttp_client().get(url)


class AioHTTPClient(HTTPClient):
    """
    A default client implemented using aiohttp.
    """
    async def get_json(self, url: str) -> Dict[str, Any]:
        """
        Get and parse JSON data from a URL.

        :param url: The URL to fetch the data from.
        :return: The JSON data as a dictionary.
        :raise JWTHTTPFetchError: If there's a problem fetching or decoding the data.
        """
        if not (url.startswith("https://")):
            raise JWTHTTPFetchError("Unsupported protocol in 'iss'")

        try:
            async with SingletonAiohttp.get(url) as response:
                if response.status != 200:
                    raise JWTHTTPFetchError(f"Failed to fetch or decode {url}")
                data = await response.json()
        except (aiohttp.ClientError, JSONDecodeError) as e:
            raise JWTHTTPFetchError(f"Failed to fetch or decode {url}") from e

        return data


class AccessToken(HTTPBearer):
    """Extract and validate JWT claims from Authorization header"""
    def __init__(self):
        super().__init__(bearerFormat="JWT")
        # Note: We pass in our own http client to avoid https://github.com/tiangolo/fastapi/issues/301
        self.fetcher = AsyncKeyFetcher(valid_issuers=[settings.auth_issuer], http_client=AioHTTPClient())

    async def __call__(
        self, request: Request
    ) -> schemas.AccessTokenData:
        bearer = await super().__call__(request)

        try:
            public_signing_key = await self.fetcher.get_key(bearer.credentials)
        except JWTHTTPFetchError as err:
            log.error("Failed to retrieve public key for the access token", details=str(err))
            raise HTTPException(
                status.HTTP_503_SERVICE_UNAVAILABLE,
                "Authorization server not reachable"
            )
        except JWTKeyFetcherError as err:
            # Either an invalid token, a token from an invalid JWKS service or a problem with the JWKS service
            log.error("Failed to retrieve public key for the access token", details=str(err))
            raise HTTPException(
                status.HTTP_401_UNAUTHORIZED,
                "Invalid access token",
                headers={"WWW-Authenticate": "Bearer"},
            )

        try:
            # Validates "nbf" (Not Before), "exp" (Expiration Time), "aud" (Audience), "iss" (Issuer)
            claims = jwt.decode(
                bearer.credentials,
                public_signing_key.key,
                algorithms=["RS256"],
                audience=settings.auth_audience
                # Note: We're not validating issuer here, since AsyncKeyFetcher already did that earlier
            )
        except jwt.exceptions.InvalidTokenError as err:
            log.error("Invalid JWT token", details=str(err))
            raise HTTPException(
                status.HTTP_401_UNAUTHORIZED,
                str(err),
                headers={"WWW-Authenticate": "Bearer"},
            )

        print(claims)

        return schemas.AccessTokenData(
            subject=claims["sub"],
            issuer=claims["iss"],
            scopes=(claims.get("scope") or "").split(" "),
            email=claims.get("https://bibsent.no/email"),
        )


def find_or_create_db_user(db: Session, user: schemas.User) -> models.User:
    db_user_identity = db.query(models.User) \
        .filter(models.User.external_id == user.subject) \
        .first()

    if db_user_identity is None:
        db_user_identity = models.User(
            external_id=user.subject,
            issuer=user.issuer,
            username=user.email
        )
        db.add(db_user_identity)

    db.commit()
    db.refresh(db_user_identity)

    return db_user_identity


access_token_scheme = AccessToken()


async def get_current_user(
    security_scopes: SecurityScopes,
    token_data: schemas.AccessTokenData = Depends(access_token_scheme)
) -> Optional[schemas.User]:
    for scope in security_scopes.scopes:
        if scope not in token_data.scopes:
            raise HTTPException(
                status_code=status.HTTP_403_FORBIDDEN,
                detail=f"Missing the '{scope}' permission",
                headers={"WWW-Authenticate": "Bearer"},
            )
    return schemas.User(**token_data.__dict__)
