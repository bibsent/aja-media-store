import sentry_sdk
import structlog
from sentry_dramatiq import DramatiqIntegration
from sentry_sdk.integrations.sqlalchemy import SqlalchemyIntegration
from app.settings import settings

log = structlog.get_logger(__name__)


def configure_sentry(traces_sample_rate=0.2):
    if settings.sentry_dsn:
        sentry_sdk.init(
            dsn=settings.sentry_dsn,
            integrations=[
                SqlalchemyIntegration(),
                DramatiqIntegration(),
            ],
            environment=settings.app_env,
            sample_rate=1,
            traces_sample_rate=traces_sample_rate,
        )
        log.info('Sentry integration enabled')
    else:
        log.info('Sentry integration not enabled, since no DSN was configured')
