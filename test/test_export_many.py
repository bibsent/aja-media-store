# Note: Fixtures are defined in conftest.py
from unittest.mock import Mock, call
import pytest
from app.tasks.registry.export_many import export_many

test_data = [
    'promus',
    'nettbutikken',
]


@pytest.mark.parametrize("test_target", test_data)
def test_export_many(test_target, mock_asset_data, registry):
    """Test that we export products not yet exported to Promus"""
    task_mock = Mock()
    registry.get = Mock(return_value=task_mock)
    export_many(target=test_target)

    expected_calls = [
        call(asset_id=asset_data["asset_id"])
        for asset_data in mock_asset_data
        if not asset_data["exported_to_" + test_target]
    ]

    assert registry.get.call_count == 1

    assert task_mock.send.call_count == len(expected_calls)
    assert task_mock.send.call_args_list == expected_calls
