# Note: Fixtures are defined in conftest.py
import uuid
from pathlib import Path
from datetime import datetime
from typing import List

import pytest
import structlog
import voluptuous
from alembic import config
from starlette import status
from starlette.exceptions import HTTPException
from voluptuous import Schema

from app import schemas
from app.main import app
from app.security import access_token_scheme
from app.settings import settings

TEST_DIR = Path(__file__).resolve().parent
cfg = config.Config(str(TEST_DIR.parent.joinpath("alembic.ini")))

test_data = [
    {   # Normal JPG image
        "ean": "9788234002809",
        "file": TEST_DIR.joinpath('9788234002809.jpg'),
        "format": "JPEG",
        "height": 637,
        "width": 400,
        "size": 50466,
        "md5": "a949cced790c08c009b28e09db846964",
    },
    {   # PNG image with RGBA
        "ean": "9788253042190",
        "file": TEST_DIR.joinpath('9788253042190.png'),
        "format": "PNG",
        "height": 321,
        "width": 300,
        "size": 257154,
        "md5": "6c90d85566ce2f1dd038c6664af7fb87",
    }
]

log = structlog.get_logger(__name__)


def override_access_token_scheme(subject: str = 'abc|123', issuer: str = None, email: str = None, scopes: List[str] = None, raise_error=False):
    def token_factory():
        if raise_error:
            raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED)

        return schemas.AccessTokenData(
            subject=subject,
            issuer=issuer or settings.auth_issuer,
            scopes=scopes or [],
            email=email,
        )
    return token_factory


def test_health_response(client):
    response = client.get("/health")
    assert response.status_code == 200
    assert response.json() == {"status": "pass"}


def valid_date(fmt='%Y-%m-%d'):
    return lambda v: datetime.strptime(v, fmt)


def valid_uuid():
    return lambda v: uuid.UUID(v)


def test_store_asset_without_auth(client):
    app.dependency_overrides[access_token_scheme] = override_access_token_scheme(raise_error=True)
    response = client.post(
        "/assets/",
        files={"file": test_data[0]["file"].open("rb")},
        headers={"apikey": "very-secret"},
        data={"ean": test_data[0]["ean"], "asset_type": "cover", "source": "publisher"}
    )

    assert response.status_code == 401


def test_store_asset_without_permission(client):
    app.dependency_overrides[access_token_scheme] = override_access_token_scheme()
    response = client.post(
        "/assets/",
        files={"file": test_data[0]["file"].open("rb")},
        headers={"apikey": "very-secret"},
        data={"ean": test_data[0]["ean"], "asset_type": "cover", "source": "publisher"}
    )

    assert response.status_code == 403


@pytest.mark.parametrize("test_asset", test_data)
def test_store_asset(client, test_asset):
    app.dependency_overrides[access_token_scheme] = override_access_token_scheme(scopes=["upload:files"])
    response = client.post(
        "/assets/",
        files={"file": test_asset["file"].open("rb")},
        headers={"apikey": "very-secret"},
        data={"ean": test_asset["ean"], "asset_type": "cover", "source": "publisher"}
    )

    assert response.status_code == 200

    schema = Schema({
        "asset_type": "cover",
        "created": valid_date("%Y-%m-%dT%H:%M:%S"),
        "format": test_asset["format"],
        "height": test_asset["height"],
        "width": test_asset["width"],
        "size": test_asset["size"],
        "md5": test_asset["md5"],
        "product": {
            "ean": test_asset["ean"],
            "uuid": valid_uuid(),
        },
        "s3_version": str,
        "source": "publisher",
        "variants": [],
        "export_status": [],
        "version": 1,
        "url": str,
    })
    schema(response.json())


@pytest.mark.parametrize("test_asset", test_data)
def test_find_asset(client, test_asset):
    app.dependency_overrides[access_token_scheme] = override_access_token_scheme(scopes=["read:metadata"])
    response = client.get("/assets/", params={"ean": test_asset["ean"]})
    schema = Schema({
        "items": [
            {
                "asset_type": "cover",
                "created": valid_date("%Y-%m-%dT%H:%M:%S"),
                "format": test_asset["format"],
                "height": test_asset["height"],
                "width": test_asset["width"],
                "size": test_asset["size"],
                "md5": test_asset["md5"],
                "product": {
                    "ean": test_asset["ean"],
                    "uuid": valid_uuid(),
                },
                "s3_version": str,
                "source": "publisher",
                "variants": [],
                "version": 1,
                "url": str,
            }
        ],
        "total": 1,
        "limit": 50,
        "offset": 0,
    })
    try:
        schema(response.json())
    except voluptuous.error.Invalid as exc:
        print("------------RESPONSE:\n", response.json(), "\n---------------------\nTEST ASSET:\n", test_asset)
        raise exc


@pytest.mark.parametrize("test_asset", test_data)
def test_get_asset_variant(client, test_asset):
    app.dependency_overrides[access_token_scheme] = override_access_token_scheme(scopes=["read:metadata"])
    response = client.get("/assets/", params={"ean": test_asset["ean"]})
    product_uuid = response.json()["items"][0]['product']['uuid']
    response = client.get("/%s/cover/original.jpg" % product_uuid)

    assert response.ok
    assert response.headers["Content-Type"] == "image/jpeg"
