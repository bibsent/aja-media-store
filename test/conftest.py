import sys
from datetime import datetime
from pathlib import Path
import pytest
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from alembic.command import upgrade as alembic_upgrade
from alembic.command import downgrade as alembic_downgrade

from alembic.config import Config as AlembicConfig
from fastapi.testclient import TestClient

from app import models
from app.console import configure_s3_bucket_cmd
from app.main import app
from app.settings import settings

TEST_DIR = Path(__file__).resolve().parent
alembic_ini = str(TEST_DIR.parent.joinpath("alembic.ini"))


@pytest.fixture(autouse=True, scope='session')
def bucket():
    _bucket = configure_s3_bucket_cmd([])
    yield _bucket


@pytest.fixture(autouse=True, scope='module')
def db():
    engine = create_engine(settings.db_url, echo=True)
    session_factory = sessionmaker(bind=engine)
    _db = {
        'engine': engine,
        'session_factory': session_factory,
    }
    alembic_config = AlembicConfig(alembic_ini)
    alembic_config.set_main_option("sqlalchemy.url", settings.db_url)
    alembic_upgrade(alembic_config, "head")
    sys.stderr.write('\n----- RUN ALEMBIC MIGRATION\n')
    yield _db
    sys.stderr.write('\n----- DB TEARDOWN\n')
    alembic_downgrade(alembic_config, "base")
    engine.dispose()


@pytest.fixture(scope='function')
def session(db):
    session = db['session_factory']()
    yield session
    sys.stderr.write('\n----- CREATE DB SESSION\n')

    session.rollback()
    session.close()
    sys.stderr.write('\n----- ROLLBACK DB SESSION\n')


@pytest.fixture
def client():
    return TestClient(app)


@pytest.fixture(autouse=True, scope="module")
def mock_asset_data(db):
    assets = []
    with db['session_factory']() as session:
        # Simulate that we have two products
        db_products = [
            models.Product(uuid="a08c85d4-c067-4851-ba41-e671a42d4006", ean="9788244624008"),
            models.Product(uuid="182d2218-abc5-464f-9377-d6d1512a674e", ean="9788205404649")
        ]
        # Each having one asset
        db_assets = [
            models.Asset(
                product=db_product,
                asset_type="cover",
                created=datetime.now(),
                version=1,
                s3_version="blablabla",
                md5="blablabla",
                source="misc",
                format="JPEG",
                width=200,
                height=300,
                size=4000,
                uploader=None,
            )
            for db_product in db_products
        ]
        # One exported to Promus
        models.ExportStatus(product=db_products[0], target="promus", date=datetime.now())
        products_exported_to_promus = [db_products[0]]

        # None exported to Nettbutikken
        exported_to_nettbutikken = []

        for db_product in db_products:
            session.add(db_product)
        session.commit()
        for db_asset in db_assets:
            exported_to_promus = True if db_asset.product in products_exported_to_promus else False
            print(exported_to_promus)
            session.refresh(db_asset)
            assets.append({
                "asset_id": db_asset.id,
                "exported_to_promus": exported_to_promus,
                "exported_to_nettbutikken": exported_to_nettbutikken,
            })

    return assets


@pytest.fixture(scope="module")
def registry():
    from app.tasks import registry
    return registry.load_all(False)
