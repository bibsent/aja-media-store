"""Add products.created

Revision ID: ef5ef7239026
Revises: d2119d17dc85
Create Date: 2021-06-23 23:51:26.660364

"""
from alembic import op
import sqlalchemy as sa

# revision identifiers, used by Alembic.
revision = 'ef5ef7239026'
down_revision = 'd2119d17dc85'
branch_labels = None
depends_on = None


def upgrade():
    op.create_index(op.f('ix_asset_created'), 'asset', ['created'], unique=False)
    op.add_column('product', sa.Column('created', sa.DATETIME(), nullable=True))
    op.create_index(op.f('ix_product_created'), 'product', ['created'], unique=False)


def downgrade():
    op.drop_index(op.f('ix_product_created'), table_name='product')
    op.drop_column('product', 'created')
    op.drop_index(op.f('ix_asset_created'), table_name='asset')
