"""Add date started to import_status

Revision ID: d28c402b03c3
Revises: ef5ef7239026
Create Date: 2021-09-12 16:03:50.277769

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd28c402b03c3'
down_revision = 'ef5ef7239026'
branch_labels = None
depends_on = None


def upgrade():
    op.alter_column("import_status",
                    column_name="date",
                    new_column_name="completed_at",
                    existing_type=sa.DATETIME(),
                    existing_nullable=False)
    op.add_column("import_status",
                  sa.Column('started_at', sa.DATETIME(), nullable=True))


def downgrade():
    op.alter_column("import_status",
                    column_name="completed_at",
                    new_column_name="date",
                    existing_type=sa.DATETIME(),
                    existing_nullable=False)
    op.drop_column("import_status", "started_at")
