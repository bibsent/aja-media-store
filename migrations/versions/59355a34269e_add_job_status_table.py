"""Add job status table

Revision ID: 59355a34269e
Revises: d66d6a7328e8
Create Date: 2021-09-12 19:15:20.712233

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '59355a34269e'
down_revision = '34e8e4813e9f'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'job_status',
        sa.Column('job_id', sa.Unicode(length=40), nullable=False),
        sa.Column('job_name', sa.Unicode(length=50), nullable=False),
        sa.Column('enqueued_at', sa.DATETIME(), nullable=True),
        sa.Column('started_at', sa.DATETIME(), nullable=True),
        sa.Column('ended_at', sa.DATETIME(), nullable=True),
        sa.Column('result', sa.JSON(), nullable=False, default={}),
        sa.Column('state', sa.Unicode(length=20), nullable=True),
        sa.Column('retries', sa.Integer(), nullable=False, default=0),
        sa.Column('total_processed', sa.Integer(), nullable=False, default=0),
        sa.PrimaryKeyConstraint('job_id')
    )


def downgrade():
    op.drop_table('job_status')
