"""Add export_tasks status table

Revision ID: 110b7b3a816b
Revises: 4d1d7f76fe39
Create Date: 2021-05-27 23:51:45.160002

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '110b7b3a816b'
down_revision = '4d1d7f76fe39'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'export_status',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('target', sa.Unicode(length=50), nullable=False),
        sa.Column('date', sa.DATETIME(), nullable=False),
        sa.Column('extra', sa.JSON(), nullable=False),
        sa.Column('product_id', sa.Integer(), nullable=True),
        sa.Column('asset_id', sa.Integer(), nullable=True),
        sa.ForeignKeyConstraint(['asset_id'], ['asset.id'], ondelete='CASCADE'),
        sa.ForeignKeyConstraint(['product_id'], ['product.id'], ondelete='CASCADE'),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_export_status_asset_id'), 'export_status', ['asset_id'], unique=False)
    op.create_index(op.f('ix_export_status_product_id'), 'export_status', ['product_id'], unique=False)


def downgrade():
    op.drop_constraint('export_status_ibfk_1', table_name='export_status', type_='foreignkey')
    op.drop_constraint('export_status_ibfk_2', table_name='export_status', type_='foreignkey')

    op.drop_index(op.f('ix_export_status_product_id'), table_name='export_status')
    op.drop_index(op.f('ix_export_status_asset_id'), table_name='export_status')
    op.drop_table('export_status')
