"""Add job state table

Revision ID: 1186f17a7ede
Revises: 59355a34269e
Create Date: 2021-09-12 18:51:54.514833

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '1186f17a7ede'
down_revision = '59355a34269e'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'job_state',
        sa.Column('job_name', sa.Unicode(length=50), nullable=False),
        sa.Column('state', sa.JSON(), nullable=False, default={}),
        sa.PrimaryKeyConstraint('job_name')
    )


def downgrade():
    op.drop_table('job_state')
