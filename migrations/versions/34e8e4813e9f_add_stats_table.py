"""Add stats table

Revision ID: 34e8e4813e9f
Revises: d28c402b03c3
Create Date: 2021-09-12 16:42:04.699760

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '34e8e4813e9f'
down_revision = 'd28c402b03c3'
branch_labels = None
depends_on = None


def upgrade():
    op.create_table(
        'stats',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('date', sa.DATETIME(), nullable=False),
        sa.Column('data', sa.JSON(), nullable=False),
        sa.PrimaryKeyConstraint('id')
    )
    op.create_index(op.f('ix_stats_date'), 'stats', ['date'], unique=False)


def downgrade():
    op.drop_table('stats')
