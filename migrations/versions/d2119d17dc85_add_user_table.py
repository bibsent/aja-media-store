"""Add user_identity

Revision ID: d2119d17dc85
Revises: 110b7b3a816b
Create Date: 2021-06-20 21:15:03.191309

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'd2119d17dc85'
down_revision = '110b7b3a816b'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.create_table(
        'user',
        sa.Column('id', sa.Integer(), nullable=False),
        sa.Column('external_id', sa.Unicode(length=100), nullable=True),
        sa.Column('issuer', sa.Unicode(length=255), nullable=True),
        sa.Column('username', sa.Unicode(length=100), nullable=True),
        sa.PrimaryKeyConstraint('id')
    )
    op.add_column('asset', sa.Column('uploader_id', sa.Integer(), nullable=True))
    op.create_index(op.f('ix_asset_uploader_id'), 'asset', ['uploader_id'], unique=False)
    op.create_foreign_key(None, 'asset', 'user', ['uploader_id'], ['id'], ondelete='SET NULL')
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.drop_constraint('asset_ibfk_2', 'asset', type_='foreignkey')
    op.drop_index(op.f('ix_asset_uploader_id'), table_name='asset')
    op.drop_column('asset', 'uploader_id')
    op.drop_table('user')
    # ### end Alembic commands ###
