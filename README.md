
# Mediebrønnen / aja-media-store

- [1. Utviklingsmiljø](#1-utviklingsmiljø)
  - [1.1. Om FastAPI og async](#11-om-fastapi-og-async)
- [2. Tilgangskontroll](#2-tilgangskontroll)
  - [2.1. Roller](#21-roller)
  - [2.2. Skaffe tilgangstoken](#22-skaffe-tilgangstoken)
    - [2.2.1. Tilgang på vegne av en bruker (Authorization Code Flow)](#221-tilgang-på-vegne-av-en-bruker-authorization-code-flow)
      - [2.2.1.1. Teste tilgang med Authorization Code Flow i Postman](#2211-teste-tilgang-med-authorization-code-flow-i-postman)
    - [2.2.2. Tilgang på vegne av en applikasjon (Client Credentials Flow)](#222-tilgang-på-vegne-av-en-applikasjon-client-credentials-flow)
      - [2.2.2.1. Teste tilgang med Client Credentials Flow i Postman](#2221-teste-tilgang-med-client-credentials-flow-i-postman)
- [3. Administrere tilgang](#3-administrere-tilgang)
  - [3.1. Administrere roller for en bruker eller gruppe](#31-administrere-roller-for-en-bruker-eller-gruppe)
  - [3.2. Administrere roller for en applikasjon](#32-administrere-roller-for-en-applikasjon)
  - [3.3. Slette samtykke(r)](#33-slette-samtykker)
  - [3.4. Sikkerhet](#34-sikkerhet)
- [4. Jobber](#4-jobber)
  - [4.1. Tidsstyrte jobber](#41-tidsstyrte-jobber)
  - [4.2. Tidsgrenser for jobber](#42-tidsgrenser-for-jobber)
  - [4.3. Hvis en jobb feiler](#43-hvis-en-jobb-feiler)
  - [4.4. Oversikt over jobber](#44-oversikt-over-jobber)
    - [4.4.1. Import fra Gardners - bøker](#441-import-fra-gardners---bøker)
    - [4.4.2. Import fra Gardners - film (DVD/Blu-Ray)](#442-import-fra-gardners---film-dvdblu-ray)
    - [4.4.3. Import fra Ingram](#443-import-fra-ingram)
    - [4.4.4. Import fra Bokbasen](#444-import-fra-bokbasen)
    - [4.4.5. Import fra sftp.aja.bs.no](#445-import-fra-sftpajabsno)
    - [4.4.6. Eksport til Nettbutikken](#446-eksport-til-nettbutikken)
    - [4.4.7. Eksport til Promus](#447-eksport-til-promus)
- [5. Eksempler](#5-eksempler)
  - [5.1. Re-eksportere et sett poster til Promus](#51-re-eksportere-et-sett-poster-til-promus)
- [6. Issues](#6-issues)

<!-- Table of Contents oppdateres med Visual Studio-tillegget Markdown All in One -->

Komponenter:

- S3-kompatibel objektlagring, så vi slipper å tenke på en A disk som må utvides
  med jevne mellomrom, I/O-ytelse og alle mulige problemer som kan oppstå med
  filsystemer og disker. Objektlagring er riktignok ikke det raskeste 
  alternativet, men hvis vi skulle ønske å redusere responstiden kan vi leggge 
  på en cache eller CDN foran (Cloudflare?)

- MySQL-database som lager for metadata og import/eksport-status. Eneste grunn
  til at vi bruker akkurat MySQL er at vi allerede bruker det for Pimcore, og
  allerede har et backup-opplegg for den, men vi kan bytte til noe annet hvis vi
  går lei. Vi lagrer også metadata i S3, så det er mulig å gjenopprette
  databasen i et worst-case-scenario.

- API basert på Python-rammeverket [FastAPI](https://fastapi.tiangolo.com/).

- Meldingskø basert på [Dramatiq](https://dramatiq.io/) og
  [RabbitMQ](https://www.rabbitmq.com/).

Enkel systemskisse:

![Systemskisse](./systemskisse.svg)

Systemet har en tjenesteorientert arkitektur, der prosessene bare kommuniserer med hverandre via meldingskøen. API-et bryr seg f.eks. ikke om hvor arbeidsprosessene kjører, så lenge begge kan kommunisere med den samme meldingskøen. Dette gir fleksibilitet til å skalere og deploye prosesser uavhengig av hverandre.

Prinsipper:

- Vi lagrer originalen som lastes opp, og genererer varianter etter behov.

- S3-bøtten er satt opp med med *bucket versioning* og *lifecycle policiees* som
  sørger for at gamle versjoner slettes etter et visst antall dager.
  Se `app/console.py` for detaljer.

- Strukturen er lagt opp så det skal være mulig å hente en variant uten å
  kontakte databasen, med unntak av første gang den hentes.

```
/originals
  /{product uuid}
    /{asset_type}    # f.eks. 'cover'
/variants
  /{product uuid}
    /original.jpg    # stor variant av filen som jpeg
    /thumbnail.jpg   # standard thumbnailstørrelse
    /h200.jpg        # Thumbnail med høyde 200px
    [osv...]
```

## 1. Utviklingsmiljø

Du trenger [Docker](https://www.docker.com/),
[Poetry](https://python-poetry.org/docs/),
[Lefthook](https://github.com/evilmartians/lefthook) og
[Hadolint](https://github.com/hadolint/hadolint):

    git clone git@gitlab.com:bibsent/aja-media-store.git
    cd aja-media-store
    lefthook install

For å starte aja-media-store lokalt:

    $ docker-compose up -d

Dette starter

- MariaDB på port 3306 med pålogging `media_store` / `media_store`
- Minio: http://localhost:9000 med pålogging: `minioadmin`
/ `minioadmin`
- RabbitMQ: http://localhost:15672 med pålogging `guest` / `guest`
- aja-media-store på http://localhost:5000

Selv om alt kan kjøres i Docker, kan det være praktisk å  installere 
Python-avhengigheter lokalt også for bedre IDE-støtte.
Med [Poetry](https://python-poetry.org/):

    $ poetry install

Hvis installasjonen av mysqlclient feiler, trenger du MariaDB development files
(Debian/Ubuntu: `apt install libmariadb-dev`).

Innstillinger settes gjennom miljøvariabler, f.eks. i en `.env`-fil:

    APP_ENV=dev

    DB_HOST=127.0.0.1
    DB_USER=media_store
    DB_PASSWORD=media_store
    DB_DATABASE=media_store
    
    RABBITMQ_URL=amqp://guest:guest@localhost:5672

    S3_ENDPOINT_URL=http://localhost:9000
    S3_REGION=home
    S3_ACCESS_KEY=minioadmin
    S3_ACCESS_SECRET=minioadmin
    S3_BUCKET=aja-media-store-test
    
    ASSET_BASE_URL=http://localhost:5000/

    AUTH_ISSUER=
    AUTH_AUDIENCE=

Legg merke til at en del standard-miljøvariabler for Docker-containerne er
definert i `docker-compose.yml`. Hvis samme variabel også er angitt i 
`.env`-fila, vil Docker Compose velge verdien fra `.env`-fila.

Sett opp S3-bøtten:

    docker-compose run api console configure_s3_bucket

### Databasemigrasjoner (Alembic)

Databasemigrasjonene kjøres automatisk når API-et starter.

For å lage en ny:

    docker-compose run api alembic revision -m "Add job status table"

og kjøre den:

    docker-compose run api alembic upgrade head

For å rulle tilbake siste migrasjon:

    docker-compose run api alembic downgrade -1

### 1.1. Om FastAPI og async

FastAPI lar oss velge per rute om vi vil bruke async eller ei.
I begynnelsen hadde jeg lyst å prøve å gjøre mest mulig asynkront, men 
*noen* av rutene må være synkrone fordi de bruker bibliotek som kun finnes i
synkron variant, f.eks. bildebiblioteket PIL.
Og hvis en har en blanding av synkrone og asynkrone ruter, må avhengigheter
som brukes av begge typene ruter, f.eks. databasetilkobling (f.eks. SQL Alchemy),
plutselig gjøres tilgjengelig i *både* synkron og asynkron variant, noe som 
fører til en god del ekstra kode og mye ekstra kompleksitet.
Og det gjør det det vanskeligere å gjenbruke kode mellom FastAPI og
Dramatiq.
Dette API-et bruker derfor i praksis bare synkrone ruter.

Se også [Not everything needs to be async](https://sgoel.dev/posts/not-everything-needs-to-be-async/)

## 2. Tilgangskontroll

For å få tilgang til tilgangsbegrensede operasjoner, må en først få et signert 
tilgangstoken (Access Token) fra autoriseringsserveren (Auth0), som en så
kan bruke som Bearer-token når en kommuniserer med aja-media-store:

![](docs/auth.svg)

Tokenet legges i `Authorization`-headeren:

    Authorization: Bearer <token>

Aja-media-store kommuniserer ikke selv med autoriseringsserveren
(utover å hente inn sertifikater), det sjekker bare at tokenet er gyldig.
Nærmere bestemt bruker vi [PyJWT](https://pyjwt.readthedocs.io/) for å validere 

* at tokenet er signert med riktig sertifikat (fra https://biblioteksentralen.eu.auth0.com/.well-known/jwks.json)
* at vi er innenfor tidsområdet det gjelder for ("nbf" og "exp")
* at utsteder ("iss") stemmer med verdien i AUTH_ISSER
* at mottaker ("aud") stemmer med verdien i AUTH_AUDIENCE

Hvis det er det, stoler
Aja-media-store på informasjonen i tokenet (brukerens identitet og roller).
Aja-media-store er en [Resource Server](https://www.oauth.com/oauth2-servers/the-resource-server/) etter OAuth-terminologi.

Til slutt sjekker vi om tokenet inneholder eventuelle scopes som angir rettigheter
for å utføre operasjonen som etterspørres, f.eks. `upload:files` for å laste opp filer
eller `execute:jobs` for å kjøre jobber.

### 2.1. Rettigheter

Følgende rettigheter finnes i aja-media-store:

- `upload:files` gir tilgang til å laste opp filer.
- `execute:jobs` gir tilgang til å starte jobber.

### 2.2. Skaffe tilgangstoken

Det er to forskjellige måter å skaffe et tilgangstoken på fra Azure AD,
avhengig av om en skal utføre en handling på vegne av en bruker eller en applikasjon:

#### 2.2.1. Tilgang på vegne av en bruker (Authorization Code Flow)

Denne fremgangsmåten brukes hvis en har en frontendapplikasjon (klient) med en 
bruker som skal autentisere seg mot aja-media-store.

1. Klienten sender brukeren til en innloggingsside hos autoriseringsserveren (Azure AD)
2. Brukeren logger inn og samtykker til at informasjon kan deles med aja-media-store 
3. Klienten får tilbake et JWT-formatert [tilgangstoken](https://docs.microsoft.com/en-us/azure/active-directory/develop/access-tokens) 
   (og et gjenoppfriskningstoken / refresh token som kan brukes til å få et nytt tilgangstoken senere).
   
For en eksempelimplementasjon, se SPA-delen i 
[Vanilla JavaScript single-page application (SPA) using MSAL.js to authorize users for calling a protected web API on Azure AD](https://github.com/Azure-Samples/ms-identity-javascript-tutorial/tree/main/3-Authorization-II/1-call-api). Denne bruker MSAL.js for å ta seg av grovarbeidet i prosessen, men det burde ikke være noe problem å bruke andre OIDC-bibliotek, f.eks.
[AppAuth](https://github.com/openid/AppAuth-JS) (TODO: Test at Microsoft faktisk følger standarder!)

En må begynne med å opprette en App Registration for frontendapplikasjonen i Azure AD:

1. Gå til App Registrations > New registration
2. Under Redirect URI, velg "Single Page Application" og legg inn en URL for testing, f.eks. "http://localhost:3000". Flere URL-er kan legges til senere.

##### 2.2.1.1. Teste tilgang med Authorization Code Flow i Postman

Det kan være lurt å først lese [Postman-dokumentasjonen for OAuth 2.0](https://learning.postman.com/docs/sending-requests/authorization/#oauth-20).

Først må en registrere en redirect URL for Postman i Azure AD:

1. Åpne App Registrations og velg frontendapplikasjonen du vil teste.
2. Under Authentication, velg "Add a platform"
3. Velg "Mobile and desktop applications" og legg inn `https://oauth.pstmn.io/v1/callback`

![](docs/azure-ad-app-registration-auth.png)

Tilbake i Postman: Under Authorization-fanen, velg OAuth 2.0 fra Type-menyen
og legg inn følgende verdier:

- Grant Type: Authorization Code Flow (with PKCE)
- Authorize using browser: Nei
- Auth URL: https://login.microsoftonline.com/AZURE_TENANT_ID/oauth2/v2.0/authorize (Bytt ut AZURE_TENANT_ID med vår tenant id)
- Access Token URL: (Fylles ut automatisk)
- Client ID: Finner en på App Registrations-siden til frontendapplikasjonen
- Client Secret: La stå blankt, brukes ikke med Authorization Code Flow
- Code Verifier: (La stå blank)
- Scope: (Fylles ut automatisk)

Slik:

![](docs/postman-new-token.png)

(Her er det brukt [Postman-variabler](https://learning.postman.com/docs/sending-requests/variables/) for Tenant ID og Client ID)

Trykk "Get New Access Token" og du får opp et vanlig innloggingsvindu i et nytt vindu:

![](docs/azure-signin.png)

Fulgt av en forespørsel om samtykke, hvis du ikke tidligere har samtykket:

![](docs/azure-consent.png)

Tilbake i Postman kan du trykke "Use token", og du er klar til å bruke det:

![](docs/postman-use-token.png)

#### 2.2.2. Tilgang på vegne av en applikasjon (Client Credentials Flow)

Applikasjoner/upersonlige brukere/systembrukere som kan holde på hemmeligheter, 
kan autentisere seg med [OAuth 2.0 client credentials flow](https://docs.microsoft.com/en-us/azure/active-directory/develop/v2-oauth2-client-creds-grant-flow). 
Prosssen er:

1. Klienten identifiserer seg ovenfor autoriseringsserveren ved hjelp av enten en klienthemmelighet eller et klientsertifikat.
3. Klienten får tilbake et JWT-formatert [tilgangstoken](https://docs.microsoft.com/en-us/azure/active-directory/develop/access-tokens).

Klienthemmeligheter og -sertifikater har levetid på måneder eller år, så de må beskyttes godt. De kan ikke brukes av nettleserbaserte applikasjoner (single page applications).

TODO: Skriv ut mer.

##### 2.2.2.1. Teste tilgang med Client Credentials Flow i Postman

TODO: Skriv ut.

## 3. Administrere tilgang

### 3.1. Administrere roller for en bruker eller gruppe

For å legge til roller for en bruker eller gruppe i Azure AD:

1. Åpne "Enterprise Applications"
2. Finn "Ája: Bildebrønn" og gå til "Users and Groups"
3. Trykk på "Add Assignment" og så "Users and Groups"
4. Søk opp brukeren eller gruppen og lagre

> ![](docs/assign-user-permissions.png)

### 3.2. Administrere roller for en applikasjon

Prosessen for å administrere roller for en applikasjon er helt forskjellig fra prosessen for brukere:

1. Åpne "App Registrations".
2. Finn applikasjonen eller opprett en ny.
3. Gå til "API permissions" og "Add a permission"
4. Under "My APIs", velg "Ája: Bildebrønn" og deretter "Application permissions".
5. Velg rollen og lagre
5. Godkjenn ved å trykke på "Grant admin consent for Bibliotekeneshus"

> ![](docs/assign-application-permissions.png)

### 3.3. Slette samtykke(r)

Hvis en ønsker å teste dialogen for å avgi samtykke på nytt, må en først slette samtykket (permission grant).
Sitt eget samtykke kan en slette fra https://myapplications.microsoft.com/

Andres samtykke kan slettes fra PowerShell.

(Hvis du er på Mac, går det an å bruke PowerShell Core i stedet for PowerShell, 
men du må da først følge denne bruksanvisningen: <https://endjin.com/blog/2019/05/how-to-use-the-azuread-module-in-powershell-core>.)

I PowerShell:

    > Connect-AzureAD
    > $sp = Get-AzureADServicePrincipal -ObjectId "OBJECT_ID"

der `OBJECT_ID` byttes ut med frontendapplikasjonens Object ID under "Enterprise Applications" i Azure (ikke under "App Registrations").

For å list opp alle permission grants:

    > Get-AzureADOAuth2PermissionGrant -All $true| Where-Object { $_.clientId -eq $sp.ObjectId }

For å avgrense til en bestemt bruker:

    > Get-AzureADOAuth2PermissionGrant -All $true| Where-Object { $_.clientId -eq $sp.ObjectId -and $_.PrincipalId -eq "USER_ID" }

der `USER_ID` byttes ut med brukerens Object ID. For å slette brukerens grants:

    > Get-AzureADOAuth2PermissionGrant -All $true| Where-Object { $_.clientId -eq $sp.ObjectId -and $_.PrincipalId -eq "USER_ID" } | Remove-AzureADOAuth2PermissionGrant

Alternativt kan en slette ett bestemt permission grant:

    > Remove-AzureADOAuth2PermissionGrant -ObjectId GRANT_ID

der `GRANT_ID` er dets Object.

### 3.4. Sikkerhet

- OAuth-flyt: Tidligere brukte JavaScript-klienter ofte *Implicit Grant Flow*, som ikke er like sikkert som *Authorization Code Grant Flow*,
  fordi sistnevnte krevde en klient som kunne holde på hemmeligheter.
  [PKCE](https://oauth.net/2/pkce/) løste dette problemet, så i dag er [beste praksis](https://datatracker.ietf.org/doc/html/draft-ietf-oauth-security-topics) å bruke *Authorization Code Grant Flow med PKCE*.
  MSAL.js bruker dette ut av boksen.

- [Levetid for token](https://www.oauth.com/oauth2-servers/access-tokens/access-token-lifetime/): *Access Token* fra Azure har en levetid på 60 minutter som standard, mens *Refresh Token* har en standard levetid på 14 dager (ref. [Token lifetime behavior](https://docs.microsoft.com/en-us/azure/active-directory-b2c/configure-tokens?pivots=b2c-user-flow#token-lifetime-behavior)).
  Merkelig nok kan MSAL.js per juni 2021 ikke konfigureres til å ikke bruke Refresh Token: <https://github.com/AzureAD/microsoft-authentication-library-for-js/issues/3649>.

- Replay attacks: Hvis et token kommer på avveie, kan det brukes fra hvor som helst inntil det utløper – vi har ingen beskyttelse mot *replay attacks*. Et utkast til løsning på dette er [DPoP](https://datatracker.ietf.org/doc/html/draft-ietf-oauth-dpop)-standarden, som vi bør følge med på.

- XSS-risiko: MSAL.js lagrer token i localStorage, som [antakelig er like greit som andre steder](https://pragmaticwebsecurity.com/articles/oauthoidc/localstorage-xss.html), men en bør ha i tankene at et vellykket XSS-angrep vil kunne lekke tokener, og uten beskyttelse mot replay attacks vil de kunne brukes av angriperen. Vi må (som vanlig) passe på XSS-sanitisering av all brukerinput.

## 4. Jobber

Oppgaver som ikke trenger å kjøres umiddelbart, f.eks. import og eksportjobber,
legges i en meldingskø (RabbitMQ) for behandling av én eller flere arbeidsprosesser.
Lokalt kan en starte en arbeider slik med én prosess og én tråd:

```
dramatiq app.tasks.worker -p 1 -t 1
```

(Kommandoen er tilgjengelig hvis du er i shell der du har kjørt 
`poetry shell`)

Status for køen kan en se i admin-GUI-et til RabbitMQ på 
http://localhost:15672/#/queues/%2F/default (pålogging: guest / guest).

### 4.1. Tidsstyrte jobber

[APScheduler](https://apscheduler.readthedocs.io/) tar seg av å sette i gang
jobber på bestemte tidspunkter, litt som crontab.
APScheduler kjører ikke jobbene, den sender dem bare til jobbkøen.
Reglene for når jobber skal kjøres er definert i [scheduler.py](https://gitlab.com/bibsent/aja-media-store/-/blob/main/app/tasks/scheduler.py).

I et Poetry-shell kan du kjøre `scheduler` for å starte en lokal APScheduler-prosess.

### 4.2. Tidsgrenser for jobber

Alle jobber må ha en tidsgrense, så de ikke blir hengende i all evighet hvis noe
skulle gå galt.

* [Dramatiq Time Limit](https://dramatiq.io/guide.html#message-time-limits):
  Dramatiq has a default actor time limit of 10 minutes, which means that any actor running for longer than 10 minutes is killed with a TimeLimitExceeded error.
    * For langtkjørende jobber, setter vi denne per jobb basert på erfaring.
      Se `app/tasks/registry/gardners_import.py` for et eksempel.
      
* [RabbitMQ Delivery Acknowledgement Timeout](https://www.rabbitmq.com/consumers.html#acknowledgement-timeout):
  If a consumer does not ack its delivery for more than the timeout value,
  its channel will be closed with a PRECONDITION_FAILED channel exception.
    * I prod setter vi denne til en svært høy verdi i rabbitmq.conf

### 4.3. Hvis en jobb feiler

Hvis en jobb feiler av en eller annen grunn, prøver Dramatiq som standard å
kjøre den 20 ganger til ([max_retries](https://dramatiq.io/guide.html#message-retries)), 
før den til slutt forkaster jobben.
Hvis en jobb feiler, blir feilen sendt til [Sentry](https://sentry.io/), som sender e-postvarsel til oss.

Jobben blir dessuten lagt i en [dead letter queue](https://dramatiq.io/guide.html#dead-letters)
(se også [RabbitMQ-dok](https://www.rabbitmq.com/dlx.html)), der den blir liggende i 
7 dager (Kan endres med miljøvariabelen `dramatiq_dead_message_ttl`) før den blir slettet.

### 4.4. Oversikt over jobber

Importjobber blir vanligvis lagt i jobbkøen, men kan kjøres direkte ved å hekte
på `--no-queue`, for å enklere teste en jobb lokalt:

    console run_task {task} --no-queue

#### 4.4.1. Import fra Gardners - bøker

Gardners publiserer en pakke med omslagsbilder (ZIP-fil) for bøker hver mandag.
For å manuelt importere en ukentlig pakke fra Gardners:

    console run_task gardners_import [--date=YYYY-MM-DD]

Hvis en utelater `--date`, importeres filen fra denne uka (publiseres mandag).
Hvis en angir en dato med `--date`, importeres fila fra uka som inkluderer den angitte datoen.

Innstillinger:

* GARDNERS_HOST
* GARDNERS_USERNAME
* GARDNERS_PASSWORD

#### 4.4.2. Import fra Gardners - film (DVD/Blu-Ray)

Gardners publiserer en pakke med omslagsbilder (ZIP-fil) for film hver mandag.
For å manuelt importere en ukentlig pakke fra Gardners:

    console run_task gardners_dvd_import [--date=YYYY-MM-DD]

Hvis en utelater `--date`, importeres filen fra denne uka (publiseres mandag).
Hvis en angir en dato med `--date`, importeres fila fra uka som inkluderer den angitte datoen.

Innstillinger:

* GARDNERS_DVD_HOST
* GARDNERS_DVD_USERNAME
* GARDNERS_DVD_PASSWORD

#### 4.4.3. Import fra Ingram

Ingram publiserer en pakke med omslagsbilder (sett med ZIP-filer) hver søndag kveld.
For å manuelt importere en ukentlig pakke fra Ingram:

    console run_task ingram_import [--date=YYYY-MM-DD]

Hvis en utelater `--date`, importeres filen fra sist søndag.
Hvis en angir en dato med `--date`, importeres fila fra denne datoen.

Innstillinger:

* INGRAM_HOST
* INGRAM_USERNAME
* INGRAM_PASSWORD

#### 4.4.4. Import fra Bokbasen

Bokbasen publiserer omslagsbilder fortløpende og har et API vi kan spørre om 
nye/endrede filer siden sist eller siden en gitt dato:

    console run_task bokbasen_import [--date=YYYY-MM-DD]

Hvis en utelater `--date`, henter den bilder for poster oppdatert/endret siden sist.
Første gang en kjører importen, må en angi dato. Tilstanden lagres i MySQL-basen
(i `import_status`), så vi vet når vi skal hente fra neste gang importen kjøres.

Innstillinger:

* BOKBASEN_USERNAME
* BOKBASEN_PASSWORD

#### 4.4.5. Import fra sftp.aja.bs.no

Manuelt innsamlede omslagsbilder legges på sftp.aja.bs.no med ujevne mellomrom.
Vanligvis sletter vi bilder fra serveren etter de har blitt importert.
For å teste import lokalt uten å slette filer, kan en legge på `--no-delete`:

    console run_task aja_sftp_import --no-delete

Innstillinger:

* AJA_SFTP_HOST
* AJA_SFTP_USERNAME
* AJA_SFTP_PASSWORD

#### 4.4.6. Eksport til Nettbutikken

Hver gang vi får et nytt bilde, sender vi EAN/ISBN og URL for bildet til et GraphQL-endepunkt for nettbutikken. 

Eksport kan også startes manuelt:

    console run_task nettbutikken_export_many

Dette eksporterer alt som ligger i databasen som ikke har blitt eksportert enda.

Innstillinger:

* NETTBUTIKKEN_GRAPHQL_URL
* NETTBUTIKKEN_GRAPHQL_APIKEY
* NETTBUTIKKEN_GRAPHQL_FOLDER_ID

#### 4.4.7. Eksport til Promus

Hver gang vi får et bilde vi har lov å formidle videre, sender vi EAN/ISBN og URL for bildet til 
en SFTP-server som Promus kan plukke fra. 

Eksport kan også startes manuelt:

    console run_task promus_export_many

Dette eksporterer alt som ligger i databasen som ikke har blitt eksportert enda, men som kan eksporteres.

Innstillinger:

* PROMUS_SFTP_HOST
* PROMUS_SFTP_USER
* PROMUS_SFTP_PASSWORD
* PROMUS_SFTP_PATH

## 5. Eksempler

### 5.1. Re-eksportere et sett poster til Promus


```python
from authlib.integrations.requests_client import OAuth2Session

auth_client_id = "EDIT_ME"
auth_client_secret = "EDIT_ME"

token_endpoint = f"https://biblioteksentralen.eu.auth0.com/oauth/token"
session = OAuth2Session(
    auth_client_id,
    auth_client_secret,
    scope="execute:jobs"
)
session.fetch_token(token_endpoint, grant_type="client_credentials")

resp = session.post("https://media.aja.bs.no/jobs/", json={
    "job": "promus_export",
    "arguments": {
        "ean": "9788242966643",
    },
})
print(resp.json())
```



## 6. Issues

- API-et returnerer 403, ikke 401 ved ugyldig autentisering:
  https://github.com/tiangolo/fastapi/pull/2120

- Alfabetisk sortering i SwaggerUI: Venter på 
  https://github.com/tiangolo/fastapi/pull/2568
  
