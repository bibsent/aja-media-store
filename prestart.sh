# This script is sourced, so we cannot assume bash here

# Wait for services
DB_HOST=${DB_HOST:-db}
/app/docker/wait-for "$DB_HOST:3306" -t 30

# Run migrations
alembic upgrade head

# Configure S3 bucket
console configure_s3_bucket
